(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["account-account-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/account/auth/confirm/confirm.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/auth/confirm/confirm.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"account-pages mt-5 mb-5\">\n\n\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-8 col-lg-6 col-xl-5\">\n        <div class=\"text-center\">\n          <a href=\"/\">\n            <span><img src=\"assets/images/logo-dark.png\" alt=\"\" height=\"22\"></span>\n          </a>\n          <p class=\"text-muted mt-2 mb-4\">Responsive Admin Dashboard</p>\n        </div>\n        <div class=\"card text-center\">\n\n          <div class=\"card-body p-4\">\n\n            <div class=\"mb-4\">\n              <h4 class=\"text-uppercase mt-0\">Confirm Email</h4>\n            </div>\n            <img src=\"assets/images/mail_confirm.png\" alt=\"img\" width=\"86\" class=\"mx-auto d-block\" />\n\n            <p class=\"text-muted font-14 mt-2\"> A email has been send to your email.\n              Please check for an email from company and click on the included link to\n              reset your password. </p>\n\n            <a href=\"/\" class=\"btn btn-block btn-pink waves-effect waves-light mt-3\">Back to Home</a>\n\n          </div> <!-- end card-body -->\n        </div>\n        <!-- end card -->\n\n      </div> <!-- end col -->\n    </div>\n    <!-- end row -->\n  </div>\n  <!-- end container -->\n</div>\n<!-- end page -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/auth/login/login.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/auth/login/login.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"account-pages mt-5 mb-5\">\n\n\n    <div class=\"container\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-md-8 col-lg-6 col-xl-5\">\n                <div class=\"text-center pb-4\">\n                    <a href=\"/\">\n                        <span><img src=\"assets/images/logo-light.png\" alt=\"\" width=\"35%\"></span>\n                    </a>\n                </div>\n                <div class=\"card\">\n                    <div class=\"card-body p-4\">\n\n                        <form class=\"needs-validation\" name=\"loginForm\" [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\"\n                              novalidate>\n\n                            <app-ui-preloader [display]=\"loading\"></app-ui-preloader>\n\n                            <ngb-alert type=\"danger\" *ngIf=\"error\" [dismissible]=\"false\">{{ error }}</ngb-alert>\n\n                            <div class=\"form-group mb-2\">\n                                <label for=\"email\">E-mail</label>\n\n                                <input type=\"email\" formControlName=\"email\" class=\"form-control\"\n                                       [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\" id=\"email\"\n                                       placeholder=\"E-mail\"/>\n\n                                <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.email.errors.required\">E-mail é obrigatório</div>\n                                    <div *ngIf=\"f.email.errors.email\">O e-mail deve ser um endereço de e-mail válido\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"form-group mb-3\">\n                                <label for=\"password\">Senha</label>\n\n                                <input type=\"password\" formControlName=\"password\" class=\"form-control\"\n                                       [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" id=\"password\"\n                                       placeholder=\"Password\"/>\n\n                                <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.password.errors.required\">Senha requerida</div>\n                                </div>\n                            </div>\n\n                            <div class=\"form-group mb-0 text-center\">\n                                <button class=\"btn btn-primary btn-block\" type=\"submit\">Entrar</button>\n                            </div>\n                        </form>\n                    </div> <!-- end card-body -->\n                </div>\n                <!-- end card -->\n\n                <div class=\"row mt-3\">\n                    <div class=\"col-12 text-center\">\n                        <p>\n                            <a routerLink=\"/account/reset-password\" class=\"text-white ml-1\"><i\n                                    class=\"fa fa-lock mr-1\"></i>Esqueceu a senha?</a>\n                        </p>\n                    </div> <!-- end col -->\n                </div>\n                <!-- end row -->\n\n            </div> <!-- end col -->\n        </div>\n        <!-- end row -->\n    </div>\n    <!-- end container -->\n</div>\n<!-- end page -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/auth/passwordreset/passwordreset.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/auth/passwordreset/passwordreset.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"account-pages mt-5 mb-5\">\n\n\n    <div class=\"home-btn d-none d-sm-block\">\n        <a [routerLink]=\"['/']\"><i class=\"fas fa-home h2 text-white\"></i></a>\n    </div>\n\n    <div class=\"text-center pb-4\">\n        <a href=\"/\">\n            <span><img src=\"assets/images/logo-light.png\" alt=\"\"  width=\"11%\"></span>\n        </a>\n    </div>\n\n    <div class=\"container\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-md-8 col-lg-6 col-xl-5\">\n                <div class=\"card bg-pattern\">\n                    <div class=\"card-body p-4\">\n                        <div class=\"text-center mb-4\">\n                            <h4 class=\"text-uppercase mt-0 mb-3\">Redefinir senha\n\n                            </h4>\n                            <p class=\"text-muted mb-0 font-13\">Digite seu endereço de e-mail e enviaremos um e-mail com\n                                instruções\n                                para redefinir sua senha.</p>\n                        </div>\n\n\n                        <form class=\"needs-validation\" name=\"resetForm\" [formGroup]=\"resetForm\" (ngSubmit)=\"onSubmit()\"\n                              novalidate>\n\n                            <app-ui-preloader [display]=\"loading\"></app-ui-preloader>\n\n                            <ngb-alert type=\"danger\" *ngIf=\"error\" [dismissible]=\"false\">{{ error }}</ngb-alert>\n                            <ngb-alert type=\"success\" *ngIf=\"success\" [dismissible]=\"false\">{{ success }}</ngb-alert>\n\n                            <div class=\"form-group mb-3\" *ngIf=\"!success\">\n                                <label for=\"email\">E-mail</label>\n\n                                <input type=\"email\" formControlName=\"email\" class=\"form-control\"\n                                       [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\" id=\"email\"\n                                       placeholder=\"E-mail\"/>\n\n                                <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.email.errors.required\">E-mail é obrigatório</div>\n                                    <div *ngIf=\"f.email.errors.email\">O email deve ser um endereço de email válido</div>\n                                </div>\n                            </div>\n\n                            <div class=\"form-group mb-0 text-center\" *ngIf=\"!success\">\n                                <button class=\"btn btn-primary btn-block\" type=\"submit\">Enviar</button>\n                            </div>\n                        </form>\n\n                    </div> <!-- end card-body-->\n                </div>\n                <!-- end card -->\n\n                <div class=\"row mt-3\">\n                    <div class=\"col-12 text-center\">\n                        <p class=\"text-muted\"><a routerLink=\"/account/login\" class=\"text-white ml-1\"><b>Voltar ao\n                            login</b></a>\n                        </p>\n                    </div> <!-- end col -->\n                </div>\n                <!-- end row -->\n\n            </div> <!-- end col -->\n        </div>\n        <!-- end row -->\n    </div>\n    <!-- end container -->\n</div>\n<!-- end page -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/auth/signup/signup.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/auth/signup/signup.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"account-pages mt-5 mb-5\">\n  <div class=\"home-btn d-none d-sm-block\">\n    <a href=\"/\"><i class=\"fas fa-home h2 text-dark\"></i></a>\n  </div>\n\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-8 col-lg-6 col-xl-5\">\n\n          <div class=\"text-center\">\n            <a href=\"/\">\n              <span><img src=\"assets/images/logo-dark.png\" alt=\"\" height=\"22\" /></span>\n            </a>\n            <p class=\"text-muted mt-2 mb-4\">Responsive Admin Dashboard</p>\n          </div>\n\n        <div class=\"card\">\n          <div class=\"card-body p-4\">\n              <div class=\"text-center mb-4\">\n                  <h4 class=\"text-uppercase mt-0\">Register</h4>\n              </div>\n\n            <form class=\"needs-validation\" name=\"signupForm\" [formGroup]=\"signupForm\" (ngSubmit)=\"onSubmit()\"\n              novalidate>\n\n              <app-ui-preloader [display]=\"loading\"></app-ui-preloader>\n\n              <ngb-alert type=\"danger\" *ngIf=\"error\" [dismissible]=\"false\">{{ error }}</ngb-alert>\n\n              <div class=\"form-group mb-3\">\n                <label for=\"name\">Your name</label>\n\n                <input type=\"text\" formControlName=\"name\" class=\"form-control\"\n                  [ngClass]=\"{ 'is-invalid': submitted && f.name.errors }\" id=\"name\" placeholder=\"Enter your name\" />\n\n                <div *ngIf=\"submitted && f.name.errors\" class=\"invalid-feedback\">\n                  <div *ngIf=\"f.name.errors.required\">Name is required</div>\n                </div>\n              </div>\n\n              <div class=\"form-group mb-3\">\n                <label for=\"email\">Email</label>\n\n                <input type=\"email\" formControlName=\"email\" class=\"form-control\"\n                  [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\" id=\"email\" placeholder=\"Email\" />\n\n                <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\n                  <div *ngIf=\"f.email.errors.required\">Email is required</div>\n                  <div *ngIf=\"f.email.errors.email\">Email must be a valid email address</div>\n                </div>\n              </div>\n\n              <div class=\"form-group mb-3\">\n                <label for=\"password\">Password</label>\n\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\"\n                  [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" id=\"password\" placeholder=\"Password\" />\n\n                <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                  <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                </div>\n              </div>\n\n              <div class=\"form-group mb-0 text-center\">\n                <button class=\"btn btn-primary btn-block\" type=\"submit\">Sign Up</button>\n              </div>\n            </form>\n          </div> <!-- end card-body -->\n        </div>\n        <!-- end card -->\n\n        <div class=\"row mt-3\">\n          <div class=\"col-12 text-center\">\n            <p class=\"text-muted\">Already have account? <a routerLink=\"/account/login\" class=\"text-dark ml-1\"><b>Log\n                  In</b></a></p>\n          </div> <!-- end col-->\n        </div>\n        <!-- end row -->\n\n      </div> <!-- end col -->\n    </div>\n    <!-- end row -->\n  </div>\n  <!-- end container -->\n</div>\n<!-- end page -->"

/***/ }),

/***/ "./src/app/account/account-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AccountRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountRoutingModule", function() { return AccountRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    { path: 'auth', loadChildren: function () { return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./auth/auth.module */ "./src/app/account/auth/auth.module.ts")).then(function (m) { return m.AuthModule; }); } },
];
var AccountRoutingModule = /** @class */ (function () {
    function AccountRoutingModule() {
    }
    AccountRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AccountRoutingModule);
    return AccountRoutingModule;
}());



/***/ }),

/***/ "./src/app/account/account.module.ts":
/*!*******************************************!*\
  !*** ./src/app/account/account.module.ts ***!
  \*******************************************/
/*! exports provided: AccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return AccountModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _account_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-routing.module */ "./src/app/account/account-routing.module.ts");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/auth.module */ "./src/app/account/auth/auth.module.ts");





var AccountModule = /** @class */ (function () {
    function AccountModule() {
    }
    AccountModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _account_routing_module__WEBPACK_IMPORTED_MODULE_3__["AccountRoutingModule"],
                _auth_auth_module__WEBPACK_IMPORTED_MODULE_4__["AuthModule"]
            ]
        })
    ], AccountModule);
    return AccountModule;
}());



/***/ }),

/***/ "./src/app/account/auth/auth-routing.ts":
/*!**********************************************!*\
  !*** ./src/app/account/auth/auth-routing.ts ***!
  \**********************************************/
/*! exports provided: AuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function() { return AuthRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/account/auth/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/account/auth/signup/signup.component.ts");
/* harmony import */ var _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./confirm/confirm.component */ "./src/app/account/auth/confirm/confirm.component.ts");
/* harmony import */ var _passwordreset_passwordreset_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./passwordreset/passwordreset.component */ "./src/app/account/auth/passwordreset/passwordreset.component.ts");







var routes = [
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
    },
    {
        path: 'signup',
        component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"]
    },
    {
        path: 'confirm',
        component: _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmComponent"]
    },
    {
        path: 'reset-password',
        component: _passwordreset_passwordreset_component__WEBPACK_IMPORTED_MODULE_6__["PasswordresetComponent"]
    },
];
var AuthRoutingModule = /** @class */ (function () {
    function AuthRoutingModule() {
    }
    AuthRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AuthRoutingModule);
    return AuthRoutingModule;
}());



/***/ }),

/***/ "./src/app/account/auth/auth.module.ts":
/*!*********************************************!*\
  !*** ./src/app/account/auth/auth.module.ts ***!
  \*********************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/ui/ui.module */ "./src/app/shared/ui/ui.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "./src/app/account/auth/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/account/auth/signup/signup.component.ts");
/* harmony import */ var _auth_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth-routing */ "./src/app/account/auth/auth-routing.ts");
/* harmony import */ var _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./confirm/confirm.component */ "./src/app/account/auth/confirm/confirm.component.ts");
/* harmony import */ var _passwordreset_passwordreset_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./passwordreset/passwordreset.component */ "./src/app/account/auth/passwordreset/passwordreset.component.ts");











var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_7__["SignupComponent"], _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmComponent"], _passwordreset_passwordreset_component__WEBPACK_IMPORTED_MODULE_10__["PasswordresetComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbAlertModule"],
                _shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_5__["UIModule"],
                _auth_routing__WEBPACK_IMPORTED_MODULE_8__["AuthRoutingModule"]
            ]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/account/auth/confirm/confirm.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/account/auth/confirm/confirm.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYXV0aC9jb25maXJtL2NvbmZpcm0uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/account/auth/confirm/confirm.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/account/auth/confirm/confirm.component.ts ***!
  \***********************************************************/
/*! exports provided: ConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmComponent", function() { return ConfirmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConfirmComponent = /** @class */ (function () {
    function ConfirmComponent() {
    }
    ConfirmComponent.prototype.ngOnInit = function () {
    };
    ConfirmComponent.prototype.ngAfterViewInit = function () {
        document.body.classList.add('authentication-bg');
    };
    ConfirmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirm',
            template: __webpack_require__(/*! raw-loader!./confirm.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/auth/confirm/confirm.component.html"),
            styles: [__webpack_require__(/*! ./confirm.component.scss */ "./src/app/account/auth/confirm/confirm.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfirmComponent);
    return ConfirmComponent;
}());



/***/ }),

/***/ "./src/app/account/auth/login/login.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/account/auth/login/login.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":root ::ng-deep html {\n  overflow: hidden !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Zzb3V6YS/DgXJlYSBkZSB0cmFiYWxoby9JdGFyZ2V0L3NiZC9mcm9udGVuZC9zcmMvYXBwL2FjY291bnQvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYWNjb3VudC9hdXRoL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMkJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpyb290IDo6bmctZGVlcCBodG1se1xuICBvdmVyZmxvdzogaGlkZGVuICFpbXBvcnRhbnQ7XG59XG4iLCI6cm9vdCA6Om5nLWRlZXAgaHRtbCB7XG4gIG92ZXJmbG93OiBoaWRkZW4gIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/account/auth/login/login.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/account/auth/login/login.component.ts ***!
  \*******************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");







var LoginComponent = /** @class */ (function () {
    /**
     *
     * @param formBuilder
     * @param route
     * @param router
     * @param authenticationService
     */
    function LoginComponent(formBuilder, route, router, authenticationService, http) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.http = http;
        this.submitted = false;
        this.error = '';
        this.loading = false;
        this.carregarDados();
    }
    LoginComponent.prototype.carregarDados = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        // tslint:disable-next-line: no-string-literal
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.ngAfterViewInit = function () {
        document.body.classList.add('authentication-bg');
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * On submit form
     */
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.login(this.f.email.value, this.f.password.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (data) {
            if (data.status === 200) {
                _this.router.navigate([_this.returnUrl]);
            }
        }, function (error) {
            _this.error = error;
            _this.loading = false;
        });
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/account/auth/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/account/auth/passwordreset/passwordreset.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/account/auth/passwordreset/passwordreset.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYXV0aC9wYXNzd29yZHJlc2V0L3Bhc3N3b3JkcmVzZXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/account/auth/passwordreset/passwordreset.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/account/auth/passwordreset/passwordreset.component.ts ***!
  \***********************************************************************/
/*! exports provided: PasswordresetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordresetComponent", function() { return PasswordresetComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");






var PasswordresetComponent = /** @class */ (function () {
    function PasswordresetComponent(formBuilder, route, router, authenticationService) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.submitted = false;
        this.error = '';
        this.success = '';
        this.loading = false;
    }
    PasswordresetComponent.prototype.ngOnInit = function () {
        this.resetForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
        });
    };
    PasswordresetComponent.prototype.ngAfterViewInit = function () {
        document.body.classList.add('authentication-bg');
    };
    Object.defineProperty(PasswordresetComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () {
            return this.resetForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * On submit form
     */
    PasswordresetComponent.prototype.onSubmit = function () {
        var _this = this;
        this.success = '';
        this.submitted = true;
        // stop here if form is invalid
        if (this.resetForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.recover(this.f.email.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(function (data) {
            _this.loading = false;
            _this.error = '';
            if (data.status === 200) {
                _this.success = 'Enviamos um e-mail com um link para redefinir sua senha';
            }
            else {
                _this.success = 'Não foi possivel encontar o e-mail para enviar a nova senha';
            }
        }, function (error) {
            _this.error = error;
            _this.loading = false;
        });
    };
    PasswordresetComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] }
    ]; };
    PasswordresetComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-passwordreset',
            template: __webpack_require__(/*! raw-loader!./passwordreset.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/auth/passwordreset/passwordreset.component.html"),
            styles: [__webpack_require__(/*! ./passwordreset.component.scss */ "./src/app/account/auth/passwordreset/passwordreset.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], PasswordresetComponent);
    return PasswordresetComponent;
}());



/***/ }),

/***/ "./src/app/account/auth/signup/signup.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/account/auth/signup/signup.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYXV0aC9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/account/auth/signup/signup.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/account/auth/signup/signup.component.ts ***!
  \*********************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var SignupComponent = /** @class */ (function () {
    function SignupComponent(formBuilder, route, router) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.submitted = false;
        this.error = '';
        this.loading = false;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.signupForm = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    };
    SignupComponent.prototype.ngAfterViewInit = function () {
        document.body.classList.add('authentication-bg');
    };
    Object.defineProperty(SignupComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.signupForm.controls; },
        enumerable: true,
        configurable: true
    });
    /**
     * On submit form
     */
    SignupComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.signupForm.invalid) {
            return;
        }
        this.loading = true;
        setTimeout(function () {
            _this.loading = false;
            _this.router.navigate(['/account/confirm']);
        }, 1000);
    };
    SignupComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! raw-loader!./signup.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/auth/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/account/auth/signup/signup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ })

}]);
//# sourceMappingURL=account-account-module-es5.js.map