(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/footer/footer.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/footer/footer.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                Copyright 2019 Sociedade Brasileira de Dermatologia\n            </div>\n            <div class=\"col-md-6\">\n            </div>\n        </div>\n    </div>\n</footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/layout.component.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/layout.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Navigation Bar-->\n<header id=\"topnav\">\n    <app-topbar (settingsButtonClicked)=\"onSettingsButtonClicked()\" (mobileMenuButtonClicked)=\"onToggleMobileMenu()\">\n    </app-topbar>\n    <app-navbar [showMobileMenu]=\"showMobileMenu\"></app-navbar>\n</header>\n<!-- End Navigation Bar-->\n\n<div class=\"wrapper\">\n    <!-- content -->\n    <router-outlet></router-outlet>\n</div>\n\n<app-footer></app-footer>\n<app-rightsidebar></app-rightsidebar>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/navbar/navbar.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/navbar/navbar.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"topbar-menu\">\n    <div class=\"container-fluid\">\n        <div id=\"navigation\" [ngbCollapse]=\"!showMobileMenu\">\n            <!-- Navigation Menu-->\n            <ul class=\"navigation-menu\">\n                <li class=\"has-submenu\"><a [routerLink]=\"['/']\" class=\"side-nav-link-ref\"><i class=\"mdi mdi-view-dashboard\"></i>Dashboard</a></li>\n                <li class=\"has-submenu\"><a [routerLink]=\"['/user']\" class=\"side-nav-link-ref\"><i class=\"fa fa-user\"></i>Usuários</a></li>\n                <li class=\"has-submenu\"><a [routerLink]=\"['/service-desk']\" class=\"side-nav-link-ref\"><i class=\"fa fa-hospital\"></i>Postos de Atendimento</a></li>\n                <li class=\"has-submenu\"><a [routerLink]=\"['/diagnostics']\" class=\"side-nav-link-ref\"><i class=\"fa fa-book-medical\"></i>Diagnósticos</a></li>\n                <li class=\"has-submenu\"><a [routerLink]=\"['/import']\" class=\"side-nav-link-ref\"><i class=\"fa fa-upload\"></i>Importação</a></li>\n                <li class=\"has-submenu\"><a [routerLink]=\"['/export']\" class=\"side-nav-link-ref\"><i class=\"fa fa-download\"></i>Exportação</a></li>\n            </ul>\n            <!-- End navigation menu -->\n\n            <div class=\"clearfix\"></div>\n        </div>\n        <!-- end #navigation -->\n    </div>\n    <!-- end container -->\n</div>\n<!-- end navbar-custom -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/rightsidebar/rightsidebar.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/rightsidebar/rightsidebar.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Right Sidebar -->\n<div class=\"right-bar\" (clickOutside)=\"hide()\" [exclude]=\"'.right-bar-toggle'\">\n    <div class=\"rightbar-title\">\n        <a href=\"javascript:void(0);\" class=\"right-bar-toggle float-right\" (click)=\"hide();\">\n            <i class=\"fe-x noti-icon\"></i>\n        </a>\n        <h4 class=\"m-0 text-white\">Settings</h4>\n    </div>\n    <div class=\"slimscroll-menu\" appSlimScroll>\n        <!-- User box -->\n        <div class=\"user-box\">\n            <div class=\"user-img\">\n                <img src=\"assets/images/profile.jpg\" alt=\"user-img\" title=\"Mat Helme\" class=\"rounded-circle img-fluid\">\n                <a href=\"javascript:void(0);\" class=\"user-edit\"><i class=\"mdi mdi-pencil\"></i></a>\n            </div>\n\n            <h5><a href=\"javascript: void(0);\">Nik G. Patel</a></h5>\n            <p class=\"text-muted mb-0\"><small>Admin Head</small></p>\n        </div>\n    </div> <!-- end slimscroll-menu-->\n</div>\n<!-- /Right-bar -->\n\n<!-- Right bar overlay-->\n<div class=\"rightbar-overlay\"></div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/topbar/topbar.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/topbar/topbar.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Topbar Start -->\n<div class=\"navbar-custom\">\n  <div class=\"container-fluid\">\n    <ul class=\"list-unstyled topnav-menu float-right mb-0\">\n\n      <li class=\"dropdown notification-list\">\n        <!-- Mobile menu toggle-->\n        <a class=\"navbar-toggle nav-link\" [ngClass]=\"{'open': openMobileMenu}\" (click)=\"toggleMobileMenu($event)\" href=\"javascript: void(0);\">\n            <div class=\"lines\">\n              <span></span>\n              <span></span>\n              <span></span>\n            </div>\n          </a>\n        <!-- End mobile menu toggle-->\n      </li>\n\n     <!-- <li class=\"d-none d-sm-block\">\n        <form class=\"app-search\">\n          <div class=\"app-search-box\">\n            <div class=\"input-group\">\n              <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">\n              <div class=\"input-group-append\">\n                <button class=\"btn\" type=\"submit\">\n                  <i class=\"fe-search\"></i>\n                </button>\n              </div>\n            </div>\n          </div>\n        </form>\n      </li>-->\n\n      <!--<li class=\"dropdown notification-list\" ngbDropdown>\n        <a class=\"nav-link dropdown-toggle  waves-effect waves-light\" href=\"javascript: void(0);\" role=\"button\"\n          aria-haspopup=\"false\" aria-expanded=\"false\" ngbDropdownToggle id=\"notificationDropdown\">\n          <i class=\"fe-bell noti-icon\"></i>\n          <span class=\"badge badge-danger rounded-circle noti-icon-badge\">4</span>\n        </a>\n        <div class=\"dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg\"\n          aria-labelledby=\"notificationDropdown\" ngbDropdownMenu>\n\n          &lt;!&ndash; item&ndash;&gt;\n          <div class=\"dropdown-item noti-title\" ngbDropdownItem>\n            <h5 class=\"m-0\">\n              <span class=\"float-right\">\n                <a href=\"javascript: void(0);\" class=\"text-dark\">\n                  <small>Clear All</small>\n                </a>\n              </span>Notification\n            </h5>\n          </div>\n\n          <div id=\"notification-items\" class=\"slimscroll noti-scroll\" appSlimScroll>\n            &lt;!&ndash; item&ndash;&gt;\n            <a *ngFor=\"let notification of notificationItems;\" href=\"{{ notification.redirectTo }}\"\n              class=\"dropdown-item notify-item\" ngbDropdownItem>\n              <div class=\"notify-icon bg-soft-{{ notification.bgColor }} text-{{ notification.bgColor }}\">\n                <i class=\"{{ notification.icon }}\"></i>\n              </div>\n              <p class=\"notify-details\">{{ notification.text }}\n                <small class=\"text-muted\">{{ notification.subText }}</small>\n              </p>\n            </a>\n          </div>\n\n          &lt;!&ndash; All&ndash;&gt;\n          <a href=\"javascript:void(0);\" class=\"dropdown-item text-center text-primary notify-item notify-all\">\n            View All\n          </a>\n        </div>\n      </li>-->\n\n\n      <li class=\"dropdown notification-list\" ngbDropdown>\n        <a class=\"nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light\" ngbDropdownToggle\n          id=\"profileDropdown\" href=\"javascript: void(0);\" role=\"button\" aria-haspopup=\"false\" aria-expanded=\"false\">\n          <img src=\"assets/images/profile.jpg\" alt=\"user-image\" class=\"rounded-circle\">\n          <span class=\"pro-user-name ml-1\">\n            {{currentUser.name}} <i class=\"mdi mdi-chevron-down\"></i>\n          </span>\n        </a>\n        <div class=\"dropdown-menu dropdown-menu-right profile-dropdown\" aria-labelledby=\"profileDropdown\"\n          ngbDropdownMenu>\n\n          <!-- item-->\n<!--\n          <a href=\"javascript:void(0);\" class=\"dropdown-item notify-item\">\n            <i class=\"fe-user\"></i>\n            <span>Meu perfil</span>\n          </a>\n\n          <div class=\"dropdown-divider\"></div>\n-->\n\n          <!-- item-->\n          <a href=\"javascript:void(0);\" class=\"dropdown-item notify-item\" (click)=\"logout()\">\n            <i class=\"fe-log-out\"></i>\n            <span>Sair</span>\n          </a>\n\n        </div>\n      </li>\n\n<!--      <li class=\"dropdown notification-list\">\n        <a href=\"javascript:void(0);\" class=\"nav-link right-bar-toggle waves-effect waves-light\"\n          (click)=\"toggleRightSidebar()\">\n          <i class=\"fe-settings noti-icon\"></i>\n        </a>\n      </li>-->\n\n\n    </ul>\n\n    <!-- LOGO -->\n    <div class=\"logo-box\">\n      <a href=\"/\" class=\"logo text-center\">\n        <span class=\"logo-lg\">\n          <img src=\"assets/images/logo.png\" alt=\"\" height=\"60\">\n        </span>\n        <span class=\"logo-sm\">\n          <img src=\"assets/images/logo-sm.png\" alt=\"\" height=\"50\">\n        </span>\n        <div class=\"clearfix\"></div>\n      </a>\n    </div>\n  </div>\n</div>\n<!-- end Topbar -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard/dashboard.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard/dashboard.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n\n    <ng-container *ngIf=\"!public_route\">\n        <!-- start page title -->\n        <app-page-title></app-page-title>\n        <!-- end page title -->\n    </ng-container>\n    <div class=\"row\">\n        <div class=\"col-xl-12\">\n            <div class=\"card-box\">\n                <h4 *ngIf=\"!public_route\">BEM VINDO AO MODULO DE GESTÃO DE DIAGNÓSTICOS SBD</h4>\n                <div class=\"row\">\n                    <div class=\"col-12\">\n                        <label class=\"col-form-label\">CAMPANHAS</label>\n                        <select class=\"form-control custom-select\" [(ngModel)]=\"filter.campaign_id\"\n                            [disabled]=\"data.length === 0\">\n                            <option [ngValue]=\"null\">\n                                {{data.length === 0 ? 'CARREGANDO AGUARDE...' : 'SELECIONE UMA CAMPANHA'}}</option>\n                            <option *ngFor=\"let opt of campaings;\" [ngValue]=\"opt._id\">{{opt.title}}</option>\n                        </select>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-3\">\n                        <label class=\"col-form-label\">ESTADOS</label>\n                        <select class=\"form-control custom-select\" [(ngModel)]=\"filter.state\"\n                            [disabled]=\"filter.campaign_id === null\" (change)=\"getCities($event)\">\n                            <option [ngValue]=\"null\">SELECIONE UM ESTADO</option>\n                            <option *ngFor=\"let state of states;\">{{state}}</option>\n                        </select>\n                    </div>\n                    <div class=\"col-3\">\n                        <label class=\"col-form-label\">CIDADES</label>\n                        <select class=\"form-control custom-select\" [(ngModel)]=\"filter.city\"\n                            [disabled]=\"cities.length === 0 && campaing === null\" (change)=\"getServices()\">\n                            <option [ngValue]=\"null\">SELECIONE UM ESTADO</option>\n                            <option *ngFor=\"let city of cities;\">{{city}}</option>\n                        </select>\n                    </div>\n                    <div class=\"col-6\">\n                        <label class=\"col-form-label\">POSTOS DE ATENDIMENTO</label>\n                        <select class=\"form-control custom-select\" [(ngModel)]=\"filter.hospital_id\"\n                            [disabled]=\"filter.campaign_id === null\" (change)=\"selectPosto()\">\n                            <option [ngValue]=\"null\">SELECIONE UM POSTO DE ATENDIMENTO</option>\n                            <option *ngFor=\"let service of services;\" [ngValue]=\"service.value\">{{service.label}}\n                            </option>\n                        </select>\n                    </div>\n                </div>\n                <div class=\"row mt-2\">\n                    <div class=\"col-12 text-right\">\n                        <button class=\"btn btn-primary\" [disabled]=\"disable_search_btn\" (click)=\"report()\">\n                            <i *ngIf=\"!loading\" class=\"fa fa-search\"></i> {{label_search_btn}}\n                        </button>\n                    </div>\n                </div>\n\n            </div>\n        </div><!-- end col -->\n\n        <div class=\"col-xl-12\" *ngIf=\"retorno.ficha.length !== 0; else empty\">\n            <div class=\"card-box\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <div class=\"alert alert-info\">\n                            <h6 class=\"text-right\">Postos de Atendimento</h6>\n                            <h1 class=\"text-right\">{{retorno.total_hospital}}</h1>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"alert alert-info\">\n                            <h6 class=\"text-right\">Diagnósticos</h6>\n                            <h1 class=\"text-right\">{{retorno.total_quiz}}</h1>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"clearfix\">\n                    <div class=\"mb-4 float-left\">\n                        <h4 class=\"mb-0\">SELECIONE UMA PERGUNTA</h4>\n                        <small>E visualize as estatíticas ao lado</small>\n                    </div>\n                </div>\n\n                <div class=\"row clearfix\">\n                    <div class=\"col-3 pr-0\">\n                        <ul class=\"list-group list-group-flush\">\n                            <ng-container *ngFor=\"let quiz of retorno.ficha;\">\n                                <li class=\"list-group-item\" [class.selected]=\"quiz.selected\" (click)=\"select(quiz)\">\n                                    <i class=\"fa\" [class.fa-check]=\"quiz.selected\"></i> {{quiz.label}}</li>\n                            </ng-container>\n                        </ul>\n                    </div>\n                    <ng-container *ngIf=\"type !== null; else selectQuestion\">\n                        <div class=\"col-5\">\n                            <div class=\"jumbotron jumbotron-fluid m-0\"\n                                style=\"bottom:0%; position:absolute; top:0%; right: 2%; left: 0%; background-color: #f1f5f7 !important; padding: 0px; margin: 0px;\">\n                                <div class=\"container text-center\">\n\n                                    <ng-container>\n                                        <table class=\"table table-striped table-bordered table-sm\">\n                                            <thead>\n                                                <tr>\n                                                    <th scope=\"col\" class=\"text-left\">{{type.label}}</th>\n                                                    <th scope=\"col\" style=\"width: 1%; white-space: nowrap\">Quantidade\n                                                    </th>\n                                                    <th scope=\"col\" style=\"width: 1%; white-space: nowrap\">Porcentagem\n                                                    </th>\n                                                </tr>\n                                            </thead>\n                                            <tbody>\n\n                                                <tr *ngFor=\"let item of type.data\">\n                                                    <th scope=\"row\" class=\"text-left\">{{item.label}}</th>\n                                                    <td class=\"text-right\">{{item.total}}</td>\n                                                    <td class=\"text-right\">{{item.pct | number : '1.2-2'}}%</td>\n                                                </tr>\n                                            </tbody>\n                                            <tfoot>\n                                                <th class=\"text-left\">Total</th>\n                                                <th class=\"text-right\">{{type.abstract.total}}</th>\n                                                <th class=\"text-right\">{{type.abstract.pct}}</th>\n                                            </tfoot>\n                                        </table>\n                                    </ng-container>\n\n\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-4\">\n                            <canvas id=\"chart\" width=\"300\" height=\"250\"></canvas>\n                        </div>\n                    </ng-container>\n                    <ng-template #selectQuestion>\n                        <div class=\"col-9\">\n                            <div class=\"alert alert-warning\">\n                                Selecione ao lado uma pergunta para exibir as estatísticas\n                            </div>\n                        </div>\n                    </ng-template>\n                </div>\n            </div>\n        </div>\n        <ng-template #empty>\n\n            <div class=\"col-xl-12\">\n                <div class=\"card-box\">\n                    <div class=\"alert alert-warning\">\n                        <ng-container *ngIf=\"!loading\">\n                            Utilize o filtro acima para carregar as estatísticas.\n                        </ng-container>\n                        <ng-container *ngIf=\"loading\">\n                            Aguarde enquanto estamos carregando as estatísticas.\n                        </ng-container>\n                    </div>\n                </div>\n            </div>\n        </ng-template>\n\n    </div>\n</div> <!-- end container -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/ui/pagetitle/pagetitle.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/ui/pagetitle/pagetitle.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-12\">\n    <div class=\"page-title-box\">\n      <div class=\"page-title-right\">\n        <ol class=\"breadcrumb m-0\">\n          <li class=\"breadcrumb-item\" ngClass=\"{'active': item.active}\" *ngFor=\"let item of breadcrumbItems\">\n            <a [routerLink]=\"[item.path]\" *ngIf=\"!item.active\">{{ item.label }}</a>\n            <span *ngIf=\"item.active\">{{ item.label }}</span>\n          </li>\n        </ol>\n      </div>\n      <h4 class=\"page-title\">{{ title }}</h4>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/ui/preloader/preloader.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/ui/preloader/preloader.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"preloader\" [hidden]='!display'>\n  <div class=\"status\">\n      <div class=\"spinner-border avatar-sm text-primary m-2\" role=\"status\"></div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _core_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/guards/auth.guard */ "./src/app/core/guards/auth.guard.ts");
/* harmony import */ var _layouts_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layouts/layout.component */ "./src/app/layouts/layout.component.ts");
/* harmony import */ var _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/dashboard/dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");






const routes = [
    { path: 'account', loadChildren: () => __webpack_require__.e(/*! import() | account-account-module */ "account-account-module").then(__webpack_require__.bind(null, /*! ./account/account.module */ "./src/app/account/account.module.ts")).then(m => m.AccountModule) },
    { path: 'statistics', component: _pages_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"] },
    // tslint:disable-next-line: max-line-length
    { path: '', component: _layouts_layout_component__WEBPACK_IMPORTED_MODULE_4__["LayoutComponent"], loadChildren: () => __webpack_require__.e(/*! import() | pages-pages-module */ "pages-pages-module").then(__webpack_require__.bind(null, /*! ./pages/pages.module */ "./src/app/pages/pages.module.ts")).then(m => m.PagesModule), canActivate: [_core_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { scrollPositionRestoration: 'top' })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-adminto',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _core_helpers_error_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/helpers/error.interceptor */ "./src/app/core/helpers/error.interceptor.ts");
/* harmony import */ var _core_helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/helpers/jwt.interceptor */ "./src/app/core/helpers/jwt.interceptor.ts");
/* harmony import */ var _layouts_layouts_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./layouts/layouts.module */ "./src/app/layouts/layouts.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_helpers_read_title__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./core/helpers/read-title */ "./src/app/core/helpers/read-title.ts");
/* harmony import */ var _pages_dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/dashboard/dashboard.module */ "./src/app/pages/dashboard/dashboard.module.ts");












Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["enableProdMode"])();
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]
        ],
        imports: [
            _pages_dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_11__["DashboardModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _layouts_layouts_module__WEBPACK_IMPORTED_MODULE_7__["LayoutsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"]
        ],
        providers: [
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], useClass: _core_helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_6__["JwtInterceptor"], multi: true },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], useClass: _core_helpers_error_interceptor__WEBPACK_IMPORTED_MODULE_5__["ErrorInterceptor"], multi: true },
            _core_helpers_read_title__WEBPACK_IMPORTED_MODULE_10__["ReadTitle"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/core/class/services.ts":
/*!****************************************!*\
  !*** ./src/app/core/class/services.ts ***!
  \****************************************/
/*! exports provided: Services */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Services", function() { return Services; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");


class Services {
    constructor(http) {
        this.http = http;
        this.name = '';
    }
    /**
     *
     */
    getAll(id = null, options = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce(function (a, k) {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a;
            }, []).join('&');
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}${opt}`);
    }
    /**
     *
     * @param id
     */
    delete(id) {
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}/${id}`);
    }
    /**
     *
     * @param id
     */
    getById(id) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}/${id}`);
    }
    /**
     *
     * @param data
     */
    save(data) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}`, data);
    }
    /**
     *
     * @param id
     * @param data
     */
    update(id, data) {
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}/${id}`, data);
    }
    /**
     *
     * @param file
     * @param field
     */
    upload(file) {
        let formData = new FormData();
        formData.append('file', file);
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]();
        const options = {
            params: params,
            reportProgress: true,
        };
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/attachments`, formData, options);
    }
    /**
     *
     * @param file
     */
    delete_attach(id) {
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/attachments/${id}`);
    }
    /**
     *
     * @param data
     */
    sendCode(id) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].host}/${this.name}/send/${id}`);
    }
}
Services.ctorParameters = () => [
    null
];


/***/ }),

/***/ "./src/app/core/guards/auth.guard.ts":
/*!*******************************************!*\
  !*** ./src/app/core/guards/auth.guard.ts ***!
  \*******************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/core/services/auth.service.ts");




let AuthGuard = 
/**
 *
 */
class AuthGuard {
    constructor(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
    }
    /**
     *
     * @param route
     * @param state
     */
    canActivate(route, state) {
        const currentUser = this.authenticationService.currentUser();
        if (currentUser) {
            return true;
        }
        this.router.navigate(['/account/login']);
        return false;
    }
};
AuthGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
    /**
     *
     */
    ,
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/core/helpers/error.interceptor.ts":
/*!***************************************************!*\
  !*** ./src/app/core/helpers/error.interceptor.ts ***!
  \***************************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/core/services/auth.service.ts");





let ErrorInterceptor = class ErrorInterceptor {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    /**
     *
     * @param request
     * @param next
     */
    intercept(request, next) {
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload();
            }
            const error = err.error.message || err.statusText;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
        }));
    }
};
ErrorInterceptor.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] }
];
ErrorInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
], ErrorInterceptor);



/***/ }),

/***/ "./src/app/core/helpers/jwt.interceptor.ts":
/*!*************************************************!*\
  !*** ./src/app/core/helpers/jwt.interceptor.ts ***!
  \*************************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/core/services/auth.service.ts");



let JwtInterceptor = class JwtInterceptor {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    /**
     *
     * @param request
     * @param next
     */
    intercept(request, next) {
        const currentUser = this.authenticationService.currentUser();
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    "Authorization": `Bearer ${currentUser.token}`,
                }
            });
        }
        return next.handle(request);
    }
};
JwtInterceptor.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] }
];
JwtInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
], JwtInterceptor);



/***/ }),

/***/ "./src/app/core/helpers/read-title.ts":
/*!********************************************!*\
  !*** ./src/app/core/helpers/read-title.ts ***!
  \********************************************/
/*! exports provided: ReadTitle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReadTitle", function() { return ReadTitle; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ReadTitle = class ReadTitle {
    /**
     *
     */
    constructor() {
    }
    /**
     *
     * @param name
     */
    module(name) {
        let getMenu = JSON.parse(localStorage.getItem('menu'));
        localStorage.setItem('path', name);
        return getMenu[name];
    }
    /**
     *
     * @param name
     */
    resource(name) {
        switch (name) {
            case 'new':
                return 'Cadastrar';
                break;
            case 'edit':
                return 'Editar';
                break;
            case 'list':
                return 'Listagem';
                break;
            case 'doctor':
                return 'Médico';
                break;
            case 'diagnosticos':
                return 'Diagnósticos';
                break;
        }
        return name;
    }
};
ReadTitle = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ReadTitle);



/***/ }),

/***/ "./src/app/core/services/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/cookie.service */ "./src/app/core/services/cookie.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");






let AuthenticationService = class AuthenticationService {
    constructor(http, cookieService) {
        this.http = http;
        this.cookieService = cookieService;
    }
    /**
     * Returns the current user
     */
    currentUser() {
        if (!this.auth) {
            this.auth = JSON.parse(this.cookieService.getCookie('doctor'));
        }
        return this.auth;
    }
    /**
     * Performs the auth
     * @param email email of user
     * @param password password of user
     */
    login(email, password) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].host}/login`, { email, password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(user => {
            if (user && user.token) {
                this.auth = user.data;
                this.auth.token = user.token;
                this.cookieService.setCookie('doctor', JSON.stringify(this.auth), 1);
            }
            return user;
        }));
    }
    /**
     * Recover password user
     */
    recover(email) {
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].host}/login/recover`, { email })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(data => {
            return data;
        }));
    }
    /**
     * Logout the user
     */
    logout() {
        this.cookieService.deleteCookie('doctor');
        this.auth = null;
    }
};
AuthenticationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
], AuthenticationService);



/***/ }),

/***/ "./src/app/core/services/campaign.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/campaign.service.ts ***!
  \***************************************************/
/*! exports provided: CampaignService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignService", function() { return CampaignService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _class_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../class/services */ "./src/app/core/class/services.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");





let CampaignService = class CampaignService extends _class_services__WEBPACK_IMPORTED_MODULE_3__["Services"] {
    /**
     *
     * @param http
     */
    constructor(http) {
        super(http);
        this.name = 'campaign';
    }
    list() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].host}/${this.name}/list`);
    }
};
CampaignService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CampaignService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CampaignService);



/***/ }),

/***/ "./src/app/core/services/cookie.service.ts":
/*!*************************************************!*\
  !*** ./src/app/core/services/cookie.service.ts ***!
  \*************************************************/
/*! exports provided: CookieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return CookieService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CookieService = class CookieService {
    constructor() { }
    /**
     * Returns the cookie value by name
     * @param name cookie name
     */
    getCookie(name) {
        if (!name) {
            return null;
        }
        const nameEQ = name + '=';
        const ca = document.cookie.split(';');
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }
    /**
     * Deletes the cookie with given name
     * @param name cookie name
     * @param path path of the domain
     */
    deleteCookie(name) {
        this.setCookie(name, '', -1);
    }
    /**
     * Creates/sets the cookie
     * @param name name of cookie
     * @param value cookie value
     * @param days validity in days
     */
    setCookie(name, value, days) {
        let expires = '';
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toUTCString();
        }
        document.cookie = name + '=' + value + expires + '; path=/';
    }
};
CookieService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CookieService);



/***/ }),

/***/ "./src/app/core/services/event.service.ts":
/*!************************************************!*\
  !*** ./src/app/core/services/event.service.ts ***!
  \************************************************/
/*! exports provided: EventService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventService", function() { return EventService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let EventService = class EventService {
    constructor() {
        this.handler = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    /**
     * Broadcast the event
     * @param type type of event
     * @param payload payload
     */
    broadcast(type, payload = {}) {
        this.handler.next({ type, payload });
    }
    /**
     * Subscribe to event
     * @param type type of event
     * @param callback call back function
     */
    subscribe(type, callback) {
        return this.handler.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(event => event.type === type)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(event => event.payload))
            .subscribe(callback);
    }
};
EventService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EventService);



/***/ }),

/***/ "./src/app/core/services/hospital.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/hospital.service.ts ***!
  \***************************************************/
/*! exports provided: HospitalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HospitalService", function() { return HospitalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _class_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../class/services */ "./src/app/core/class/services.ts");




let HospitalService = class HospitalService extends _class_services__WEBPACK_IMPORTED_MODULE_3__["Services"] {
    /**
     *
     * @param http
     */
    constructor(http) {
        super(http);
        this.name = 'hospital';
    }
};
HospitalService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HospitalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], HospitalService);



/***/ }),

/***/ "./src/app/core/services/quiz.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/quiz.service.ts ***!
  \***********************************************/
/*! exports provided: QuizService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizService", function() { return QuizService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _class_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../class/services */ "./src/app/core/class/services.ts");





let QuizService = class QuizService extends _class_services__WEBPACK_IMPORTED_MODULE_4__["Services"] {
    /**
     *
     * @param http
     */
    constructor(http) {
        super(http);
        this.name = 'quiz';
    }
    /**
     *
     * @param id
     */
    delete(id) {
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/admin/${id}`);
    }
    /**
     *
     */
    listAll(id, options = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a;
            }, []).join('&');
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/all/${id}${opt}`);
    }
    filters() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/filters`);
    }
    All(options = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null) {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a;
            }, []).join('&');
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/all${opt}`);
    }
    campaing() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/campaign/list`);
    }
    report(data, options) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a;
            }, []).join('&');
        }
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/report${opt}`, data);
    }
    search(options) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null) {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a;
            }, []).join('&');
        }
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].host}/${this.name}/reporting${opt}`);
    }
};
QuizService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
QuizService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], QuizService);



/***/ }),

/***/ "./src/app/layouts/footer/footer.component.scss":
/*!******************************************************!*\
  !*** ./src/app/layouts/footer/footer.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/footer/footer.component.ts":
/*!****************************************************!*\
  !*** ./src/app/layouts/footer/footer.component.ts ***!
  \****************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layouts/footer/footer.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FooterComponent);



/***/ }),

/***/ "./src/app/layouts/layout.component.scss":
/*!***********************************************!*\
  !*** ./src/app/layouts/layout.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvbGF5b3V0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/layouts/layout.component.ts":
/*!*********************************************!*\
  !*** ./src/app/layouts/layout.component.ts ***!
  \*********************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LayoutComponent = class LayoutComponent {
    constructor() {
        this.showMobileMenu = false;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        document.body.classList.remove('authentication-bg');
    }
    /**
     * on settings button clicked from topbar
     */
    onSettingsButtonClicked() {
        document.body.classList.toggle('right-bar-enabled');
    }
    /**
     * On mobile toggle button clicked
     */
    onToggleMobileMenu() {
        this.showMobileMenu = !this.showMobileMenu;
    }
};
LayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout',
        template: __webpack_require__(/*! raw-loader!./layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/layout.component.html"),
        styles: [__webpack_require__(/*! ./layout.component.scss */ "./src/app/layouts/layout.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], LayoutComponent);



/***/ }),

/***/ "./src/app/layouts/layouts.module.ts":
/*!*******************************************!*\
  !*** ./src/app/layouts/layouts.module.ts ***!
  \*******************************************/
/*! exports provided: LayoutsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutsModule", function() { return LayoutsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var ng_click_outside__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-click-outside */ "./node_modules/ng-click-outside/lib/index.js");
/* harmony import */ var ng_click_outside__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng_click_outside__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/ui/ui.module */ "./src/app/shared/ui/ui.module.ts");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./layout.component */ "./src/app/layouts/layout.component.ts");
/* harmony import */ var _topbar_topbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./topbar/topbar.component */ "./src/app/layouts/topbar/topbar.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/layouts/navbar/navbar.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/layouts/footer/footer.component.ts");
/* harmony import */ var _rightsidebar_rightsidebar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./rightsidebar/rightsidebar.component */ "./src/app/layouts/rightsidebar/rightsidebar.component.ts");












let LayoutsModule = class LayoutsModule {
};
LayoutsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_layout_component__WEBPACK_IMPORTED_MODULE_7__["LayoutComponent"], _topbar_topbar_component__WEBPACK_IMPORTED_MODULE_8__["TopbarComponent"], _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["NavbarComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"], _rightsidebar_rightsidebar_component__WEBPACK_IMPORTED_MODULE_11__["RightsidebarComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbDropdownModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbCollapseModule"],
            ng_click_outside__WEBPACK_IMPORTED_MODULE_5__["ClickOutsideModule"],
            _shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_6__["UIModule"]
        ]
    })
], LayoutsModule);



/***/ }),

/***/ "./src/app/layouts/navbar/navbar.component.scss":
/*!******************************************************!*\
  !*** ./src/app/layouts/navbar/navbar.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/navbar/navbar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/layouts/navbar/navbar.component.ts ***!
  \****************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavbarComponent = class NavbarComponent {
    constructor() {
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        // activate menu item based on location
        const links = document.getElementsByClassName('side-nav-link-ref');
        let matchingMenuItem = null;
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < links.length; i++) {
            // tslint:disable-next-line: no-string-literal
            if (location.pathname === links[i]['pathname']) {
                matchingMenuItem = links[i];
                break;
            }
        }
        /*if (matchingMenuItem) {
            matchingMenuItem.classList.add('active');
            const parent = matchingMenuItem.parentElement;

            /!**
             * TODO: This is hard coded way of expading/activating parent menu dropdown and working till level 3.
             * We should come up with non hard coded approach
             *!/
            if (parent) {
                parent.classList.add('active');
                const parent2 = parent.parentElement;
                if (parent2) {
                    parent2.classList.add('in');
                }
                const parent3 = parent2.parentElement;
                if (parent3) {
                    parent3.classList.add('active');
                    const childAnchor = parent3.querySelector('.has-dropdown');
                    if (childAnchor) {
                        childAnchor.classList.add('active');
                    }
                }

                const parent4 = parent3.parentElement;
                if (parent4) {
                    parent4.classList.add('in');
                }
                const parent5 = parent4.parentElement;
                if (parent5) {
                    parent5.classList.add('active');
                }
            }
        }*/
        let dataMenu = {};
        document.querySelector('.navigation-menu').querySelectorAll('a').forEach((el) => {
            if (el.pathname !== undefined && el.pathname !== '/') {
                let id = el.pathname.replace('/', '');
                dataMenu[id] = el.text;
            }
        });
        localStorage.setItem('menu', JSON.stringify(dataMenu));
    }
    /**
     * On menu click
     */
    onMenuClick(event) {
        const nextEl = event.target.nextSibling;
        if (nextEl && !nextEl.classList.contains('open')) {
            const parentEl = event.target.parentNode;
            if (parentEl) {
                parentEl.classList.remove('open');
            }
            nextEl.classList.add('open');
        }
        else if (nextEl) {
            nextEl.classList.remove('open');
        }
        return false;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
], NavbarComponent.prototype, "showMobileMenu", void 0);
NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navbar',
        template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/navbar/navbar.component.html"),
        styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/layouts/navbar/navbar.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavbarComponent);



/***/ }),

/***/ "./src/app/layouts/rightsidebar/rightsidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layouts/rightsidebar/rightsidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvcmlnaHRzaWRlYmFyL3JpZ2h0c2lkZWJhci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/rightsidebar/rightsidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layouts/rightsidebar/rightsidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: RightsidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RightsidebarComponent", function() { return RightsidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RightsidebarComponent = class RightsidebarComponent {
    constructor() { }
    ngOnInit() {
    }
    /**
     * Hide the sidebar
     */
    hide() {
        document.body.classList.remove('right-bar-enabled');
    }
};
RightsidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rightsidebar',
        template: __webpack_require__(/*! raw-loader!./rightsidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/rightsidebar/rightsidebar.component.html"),
        styles: [__webpack_require__(/*! ./rightsidebar.component.scss */ "./src/app/layouts/rightsidebar/rightsidebar.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RightsidebarComponent);



/***/ }),

/***/ "./src/app/layouts/topbar/topbar.component.scss":
/*!******************************************************!*\
  !*** ./src/app/layouts/topbar/topbar.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvdG9wYmFyL3RvcGJhci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/topbar/topbar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/layouts/topbar/topbar.component.ts ***!
  \****************************************************/
/*! exports provided: TopbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopbarComponent", function() { return TopbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");




let TopbarComponent = class TopbarComponent {
    constructor(router, authService) {
        this.router = router;
        this.authService = authService;
        this.settingsButtonClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.mobileMenuButtonClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
        // get the notifications
        this.currentUser = this.authService.currentUser();
        this._fetchNotifications();
        this.openMobileMenu = false;
    }
    /**
     * Toggles the right sidebar
     */
    toggleRightSidebar() {
        this.settingsButtonClicked.emit();
    }
    /**
     * Toggle the menu bar when having mobile screen
     */
    toggleMobileMenu(event) {
        event.preventDefault();
        this.openMobileMenu = !this.openMobileMenu;
        this.mobileMenuButtonClicked.emit();
    }
    /**
     * Logout the user
     */
    logout() {
        this.authService.logout();
        this.router.navigate(['/account/login']);
    }
    /**
     * Fetches the notification
     * Note: For now returns the hard coded notifications
     */
    _fetchNotifications() {
        this.notificationItems = [{
                text: 'Caleb Flakelar commented on Admin',
                subText: '1 min ago',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'primary',
                redirectTo: '/notification/1'
            },
            {
                text: 'New user registered.',
                subText: '5 min ago',
                icon: 'mdi mdi-account-plus',
                bgColor: 'info',
                redirectTo: '/notification/2'
            },
            {
                text: 'Cristina Pride',
                subText: 'Hi, How are you? What about our next meeting',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'success',
                redirectTo: '/notification/3'
            },
            {
                text: 'Caleb Flakelar commented on Admin',
                subText: '2 days ago',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'danger',
                redirectTo: '/notification/4'
            },
            {
                text: 'Caleb Flakelar commented on Admin',
                subText: '1 min ago',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'primary',
                redirectTo: '/notification/5'
            },
            {
                text: 'New user registered.',
                subText: '5 min ago',
                icon: 'mdi mdi-account-plus',
                bgColor: 'info',
                redirectTo: '/notification/6'
            },
            {
                text: 'Cristina Pride',
                subText: 'Hi, How are you? What about our next meeting',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'success',
                redirectTo: '/notification/7'
            },
            {
                text: 'Caleb Flakelar commented on Admin',
                subText: '2 days ago',
                icon: 'mdi mdi-comment-account-outline',
                bgColor: 'danger',
                redirectTo: '/notification/8'
            }];
    }
};
TopbarComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], TopbarComponent.prototype, "settingsButtonClicked", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], TopbarComponent.prototype, "mobileMenuButtonClicked", void 0);
TopbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-topbar',
        template: __webpack_require__(/*! raw-loader!./topbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/topbar/topbar.component.html"),
        styles: [__webpack_require__(/*! ./topbar.component.scss */ "./src/app/layouts/topbar/topbar.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
], TopbarComponent);



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 224px;\n}\n\n#activity-container {\n  height: 168px;\n}\n\n.display-4 {\n  display: block;\n}\n\n.display-4 small {\n  cursor: default;\n  display: block;\n  font-size: 18px;\n}\n\n.list-group-item {\n  cursor: pointer;\n}\n\n.selected {\n  background-color: #f1f5f7 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Zzb3V6YS/DgXJlYSBkZSB0cmFiYWxoby9JdGFyZ2V0L3NiZC9mcm9udGVuZC9zcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFBO0FDQ0Y7O0FERUE7RUFDRSxhQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0FDQ0Y7O0FEQ0U7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNDSjs7QURJQTtFQUNFLGVBQUE7QUNERjs7QURJQTtFQUNFLG9DQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYWdtLW1hcCB7XG4gIGhlaWdodDogMjI0cHg7XG59XG5cbiNhY3Rpdml0eS1jb250YWluZXIge1xuICBoZWlnaHQ6IDE2OHB4O1xufVxuXG4uZGlzcGxheS00IHtcbiAgZGlzcGxheTogYmxvY2s7XG5cbiAgc21hbGwge1xuICAgIGN1cnNvcjogZGVmYXVsdDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmb250LXNpemU6IDE4cHg7XG4gIH1cbn1cblxuXG4ubGlzdC1ncm91cC1pdGVtIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uc2VsZWN0ZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmNWY3ICFpbXBvcnRhbnQ7XG59XG4iLCJhZ20tbWFwIHtcbiAgaGVpZ2h0OiAyMjRweDtcbn1cblxuI2FjdGl2aXR5LWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTY4cHg7XG59XG5cbi5kaXNwbGF5LTQge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5kaXNwbGF5LTQgc21hbGwge1xuICBjdXJzb3I6IGRlZmF1bHQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbi5saXN0LWdyb3VwLWl0ZW0ge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5zZWxlY3RlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWY1ZjcgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: depositCreate, DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "depositCreate", function() { return depositCreate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_services_event_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/services/event.service */ "./src/app/core/services/event.service.ts");
/* harmony import */ var _core_services_campaign_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/campaign.service */ "./src/app/core/services/campaign.service.ts");
/* harmony import */ var _core_services_hospital_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/services/hospital.service */ "./src/app/core/services/hospital.service.ts");
/* harmony import */ var _core_services_quiz_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/services/quiz.service */ "./src/app/core/services/quiz.service.ts");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");








class depositCreate {
}
let DashboardComponent = class DashboardComponent {
    constructor(eref, http, eventService, service, serviceDesk, srvQuiz) {
        this.eref = eref;
        this.http = http;
        this.eventService = eventService;
        this.service = service;
        this.serviceDesk = serviceDesk;
        this.srvQuiz = srvQuiz;
        this.campaings = [];
        this.states = [];
        this.cities = [];
        this.services = [];
        this.posto = {};
        this.data = [];
        this.filter = {
            campaign_id: null,
            state: null,
            city: null,
            hospital_id: null
        };
        this.diagnostics = [];
        this.campaing = null;
        this.type = null;
        this.consult = null;
        this.retorno = {
            ficha: []
        };
        this.loading = false;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.eventService.broadcast('changePageHeading', {
                title: 'Dashboard',
                breadcrumb: [{ label: 'SBD', active: true }, { label: 'Dashboard', active: true }]
            });
        });
    }
    ngAfterViewInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.service.list().subscribe((result) => {
                this.campaings = result.data;
            });
            this.serviceDesk.getAll().subscribe((result) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.data = result.data;
                this.getStates();
                this.services = result.data.map(n => {
                    return { value: n._id, label: n.name };
                }).sort();
            }));
        });
    }
    getStates() {
        this.cities = [];
        this.states = Array.from(new Set(this.data.map(c => c.state))).sort();
    }
    getCities(e) {
        this.cities = Array.from(new Set(this.data.filter(c => c.state === e.target.value).map(n => n.city))).sort();
        if (this.filter.state === null) {
            this.services = Array.from(new Set(this.data.map(n => {
                return { value: n._id, label: n.name };
            }))).sort();
        }
        else {
            this.services = Array.from(new Set(this.data.filter(c => c.state === this.filter.state).map(n => {
                return { value: n._id, label: n.name };
            }))).sort();
        }
        this.filter.city = null;
    }
    getServices() {
        if (this.filter.city === null) {
            this.services = Array.from(new Set(this.data.filter(c => c.state === this.filter.state).map(n => {
                return { value: n._id, label: n.name };
            }))).sort();
        }
        else {
            this.services = Array.from(new Set(this.data.filter(c => c.state === this.filter.state && c.city === this.filter.city).map(n => {
                return { value: n._id, label: n.name };
            }))).sort();
        }
        this.filter.hospital_id = null;
    }
    report() {
        this.loading = true;
        this.srvQuiz.search(this.filter).subscribe(data => {
            this.retorno = data;
            this.loading = false;
        });
    }
    get disable_search_btn() {
        return this.filter.campaign_id === null || this.loading;
    }
    get label_search_btn() {
        return this.loading ? 'CARREGANDO...' : 'PESQUISAR';
    }
    select(item) {
        this.retorno.ficha = this.retorno.ficha.map(m => {
            m.selected = false;
            return m;
        });
        item.selected = true;
        this.type = item;
        setTimeout(() => {
            this.charts(item.chart);
        }, 1000);
    }
    get public_route() {
        return parent.location.pathname.indexOf('statistics') !== -1;
    }
    countDiagnostic(item) {
        if (this.consult !== null) {
            return this.consult.filter(f => f[this.type.name] === item.value).length;
        }
        else {
            return 0;
        }
    }
    percentDiagnostic(item) {
        try {
            if (this.consult.length !== 0) {
                let total = this.consult.length;
                return Math.floor(this.consult.filter(f => f[this.type.name] === item.value).length / total * 100) + '%';
            }
            else {
                return `0%`;
            }
        }
        catch (e) {
        }
    }
    selectPosto() {
        this.diagnostics = [];
        this.posto = this.services.find(f => f.value === this.filter.hospital_id).label;
    }
    charts(chart) {
        this.canvas = document.getElementById('chart');
        this.ctx = this.canvas.getContext('2d');
        if (this.chart) {
            this.chart.reset();
        }
        this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_6__["Chart"](this.ctx, {
            type: 'pie',
            data: chart,
            options: {
                responsive: true,
            }
        });
    }
};
DashboardComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] },
    { type: _core_services_event_service__WEBPACK_IMPORTED_MODULE_2__["EventService"] },
    { type: _core_services_campaign_service__WEBPACK_IMPORTED_MODULE_3__["CampaignService"] },
    { type: _core_services_hospital_service__WEBPACK_IMPORTED_MODULE_4__["HospitalService"] },
    { type: _core_services_quiz_service__WEBPACK_IMPORTED_MODULE_5__["QuizService"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard/dashboard.component.html"),
        styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/pages/dashboard/dashboard.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"], _core_services_event_service__WEBPACK_IMPORTED_MODULE_2__["EventService"], _core_services_campaign_service__WEBPACK_IMPORTED_MODULE_3__["CampaignService"], _core_services_hospital_service__WEBPACK_IMPORTED_MODULE_4__["HospitalService"], _core_services_quiz_service__WEBPACK_IMPORTED_MODULE_5__["QuizService"]])
], DashboardComponent);



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");
/* harmony import */ var src_app_shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/ui/ui.module */ "./src/app/shared/ui/ui.module.ts");






let DashboardModule = class DashboardModule {
};
DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_shared_ui_ui_module__WEBPACK_IMPORTED_MODULE_5__["UIModule"]
        ],
        exports: [_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"]]
    })
], DashboardModule);



/***/ }),

/***/ "./src/app/shared/ui/pagetitle/pagetitle.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/shared/ui/pagetitle/pagetitle.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC91aS9wYWdldGl0bGUvcGFnZXRpdGxlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/ui/pagetitle/pagetitle.component.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/ui/pagetitle/pagetitle.component.ts ***!
  \************************************************************/
/*! exports provided: PagetitleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagetitleComponent", function() { return PagetitleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_services_event_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/services/event.service */ "./src/app/core/services/event.service.ts");



let PagetitleComponent = class PagetitleComponent {
    constructor(eventService) {
        this.eventService = eventService;
        this.eventService.subscribe('changePageHeading', (data) => {
            this.breadcrumbItems = [];
            this.title = data.title;
            this.breadcrumbItems = data.breadcrumb;
        });
    }
    ngOnInit() {
    }
};
PagetitleComponent.ctorParameters = () => [
    { type: _core_services_event_service__WEBPACK_IMPORTED_MODULE_2__["EventService"] }
];
PagetitleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-page-title',
        template: __webpack_require__(/*! raw-loader!./pagetitle.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/ui/pagetitle/pagetitle.component.html"),
        styles: [__webpack_require__(/*! ./pagetitle.component.scss */ "./src/app/shared/ui/pagetitle/pagetitle.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_services_event_service__WEBPACK_IMPORTED_MODULE_2__["EventService"]])
], PagetitleComponent);



/***/ }),

/***/ "./src/app/shared/ui/preloader/preloader.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/shared/ui/preloader/preloader.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC91aS9wcmVsb2FkZXIvcHJlbG9hZGVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/ui/preloader/preloader.component.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/ui/preloader/preloader.component.ts ***!
  \************************************************************/
/*! exports provided: PreloaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreloaderComponent", function() { return PreloaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PreloaderComponent = class PreloaderComponent {
    constructor() {
        this.display = false;
    }
    ngOnInit() {
    }
    /**
     * Shows the loader
     */
    show() {
        this.display = true;
    }
    /**
     * Hides the loader
     */
    hide() {
        this.display = false;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], PreloaderComponent.prototype, "display", void 0);
PreloaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ui-preloader',
        template: __webpack_require__(/*! raw-loader!./preloader.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/ui/preloader/preloader.component.html"),
        styles: [__webpack_require__(/*! ./preloader.component.scss */ "./src/app/shared/ui/preloader/preloader.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PreloaderComponent);



/***/ }),

/***/ "./src/app/shared/ui/slimscroll.directive.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/ui/slimscroll.directive.ts ***!
  \***************************************************/
/*! exports provided: SlimscrollDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlimscrollDirective", function() { return SlimscrollDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var smooth_scrollbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! smooth-scrollbar */ "./node_modules/smooth-scrollbar/index.js");



let SlimscrollDirective = class SlimscrollDirective {
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    ngAfterViewInit() {
        // smooth scroll
        smooth_scrollbar__WEBPACK_IMPORTED_MODULE_2__["default"].init(this.el.nativeElement);
    }
};
SlimscrollDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
];
SlimscrollDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appSlimScroll]'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
], SlimscrollDirective);



/***/ }),

/***/ "./src/app/shared/ui/ui.module.ts":
/*!****************************************!*\
  !*** ./src/app/shared/ui/ui.module.ts ***!
  \****************************************/
/*! exports provided: UIModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return UIModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var ng_click_outside__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-click-outside */ "./node_modules/ng-click-outside/lib/index.js");
/* harmony import */ var ng_click_outside__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng_click_outside__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _slimscroll_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./slimscroll.directive */ "./src/app/shared/ui/slimscroll.directive.ts");
/* harmony import */ var _preloader_preloader_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./preloader/preloader.component */ "./src/app/shared/ui/preloader/preloader.component.ts");
/* harmony import */ var _pagetitle_pagetitle_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pagetitle/pagetitle.component */ "./src/app/shared/ui/pagetitle/pagetitle.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");










let UIModule = class UIModule {
};
UIModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_slimscroll_directive__WEBPACK_IMPORTED_MODULE_6__["SlimscrollDirective"], _preloader_preloader_component__WEBPACK_IMPORTED_MODULE_7__["PreloaderComponent"], _pagetitle_pagetitle_component__WEBPACK_IMPORTED_MODULE_8__["PagetitleComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            ng_click_outside__WEBPACK_IMPORTED_MODULE_5__["ClickOutsideModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbCollapseModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbDatepickerModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbTimepickerModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["RouterModule"]
        ],
        exports: [_slimscroll_directive__WEBPACK_IMPORTED_MODULE_6__["SlimscrollDirective"], _preloader_preloader_component__WEBPACK_IMPORTED_MODULE_7__["PreloaderComponent"], _pagetitle_pagetitle_component__WEBPACK_IMPORTED_MODULE_8__["PagetitleComponent"]]
    })
], UIModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    // host: 'https://sgi-api-sbd.herokuapp.com/api',
    host: 'https://sbd-dezembro.parser-server.itarget.com.br/api',
    // host: 'http://192.168.0.2:8000/api',
    // host: 'http://localhost:8010/api',
    cep: 'https://viacep.com.br/ws/',
    name: 'SBD',
    campaing_id: '5dcb4861129b1417f48768cb'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/fsouza/Área de trabalho/Itarget/sbd/frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map