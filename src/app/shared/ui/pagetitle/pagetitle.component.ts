import {Component, OnInit, Input} from '@angular/core';
import {EventService} from "../../../core/services/event.service";

@Component({
    selector: 'app-page-title',
    templateUrl: './pagetitle.component.html',
    styleUrls: ['./pagetitle.component.scss']
})
export class PagetitleComponent implements OnInit {

    breadcrumbItems: Array<{}>;
    title: string;

    constructor(private eventService: EventService) {
        this.eventService.subscribe('changePageHeading', (data) => {
            this.breadcrumbItems = [];
            this.title = data.title;
          this.breadcrumbItems = data.breadcrumb;
        });
    }

    ngOnInit() {
    }
}
