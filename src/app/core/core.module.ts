import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './components/form/form.component';
import {RouterModule} from "@angular/router";
import {UIModule} from "../shared/ui/ui.module";

@NgModule({
  declarations: [],
  exports: [
  ],
  imports: [
    CommonModule,
    RouterModule,
    UIModule
  ]
})
export class CoreModule { }

