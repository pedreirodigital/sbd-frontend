export class Hospital {
    id: number;
    name: string;
    cep: number;
    place: string;
    district: number;
    city: boolean;
    state: string;
    number: string;
    complement: string;
    phone: number;
    email: string;
    site: string;

    accredited: number;
    public_service: number;
    service_forecast: string;
    volunteer_forecasting: string;
    cnpcp: number;
    attendance: number;
    resolutive: number;
    forwarding: string;
    lecture: string;
    surgery: string;
    comments: string;
    coordinators: string;

    responsible_name: string;
    responsible_phone: string;
    responsible_email: string;

    coordinator_name: string;
    coordinator_phone: string;
    coordinator_email: string;

    status: number;
}
