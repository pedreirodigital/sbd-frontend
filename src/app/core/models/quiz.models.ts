export class Quiz {
    hospital_id: string;
    doctor_id: string;
    identificacao: string;
    nome: string;
    idade: number;
    sexo: number;
    foto_tipo: number;
    exposicao_sol: number;
    historico_ca_pele: number;
    historico_familia_ca_pele: number;
    pessoa_risco_ca_pele: number;
    como_soube_campanha: number;
    diagnostico_clinico: number;
    evelocao: number;
    localizacao: number;
    conduta: number;
    campanha: string;
}
