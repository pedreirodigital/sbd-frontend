export class Doctor {
    id: number;
    hospitais: string;
    photo: string;
    name: string;
    crm: string;
    phone: number;
    email: boolean;
    password: string;
    token: string;
    admin: boolean;
}
