export class User {
    id: number;
    email: string;
    password: string;
    name: string;
    status: number;
    master: boolean;
    token: string;
}
