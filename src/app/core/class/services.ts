import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";
import { environment } from "../../../environments/environment";

export class Services {

    name = '';

    constructor(public http) {
    }

    /**
     *
     */
    getAll(id = null, options: any = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce(function (a, k) {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a
            }, []).join('&')
        }
        return this.http.get(`${environment.host}/${this.name}${opt}`);
    }

    /**
     *
     * @param id
     */
    delete(id) {
        return this.http.delete(`${environment.host}/${this.name}/${id}`);
    }

    /**
     *
     * @param id
     */
    getById(id) {
        return this.http.get(`${environment.host}/${this.name}/${id}`);
    }

    /**
     *
     * @param data
     */
    save(data) {
        return this.http.post(`${environment.host}/${this.name}`, data);
    }

    /**
     *
     * @param id
     * @param data
     */
    update(id, data) {
        return this.http.put(`${environment.host}/${this.name}/${id}`, data);
    }

    /**
     *
     * @param file
     * @param field
     */
    upload(file) {
        let formData = new FormData();
        formData.append('file', file);
        let params = new HttpParams();
        const options = {
            params: params,
            reportProgress: true,
        };

        return this.http.post(`${environment.host}/attachments`, formData, options);
    }

    /**
     *
     * @param file
     */
    delete_attach(id) {
        return this.http.delete(`${environment.host}/attachments/${id}`);
    }

    /**
     *
     * @param data
     */
    sendCode(id) {
        return this.http.get(`${environment.host}/${this.name}/send/${id}`);
    }
}
