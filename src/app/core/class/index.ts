import {ElementRef, Injectable} from '@angular/core';
import Swal from "sweetalert2";
import {environment} from "../../../environments/environment";

export class Index {

    data: any = [];
    selects: any = [];
    loading = true;

    module = '';
    resource = '';

    _new = true;
    _edit = true;
    _delete = true;
    _back = false;

    _nav = true;
    _limit = true;

    _id = null;

    params:any = {size: 10, page: 0};
    limit = 0;
    length = 0;

    /**
     *
     * @param event
     * @param service
     * @param router
     * @param title
     */

    constructor(public event, public service, public router, public title) {
        let path = this.router.routerState.snapshot.url.split('/');
        this.module = path[1] || 'dashboard';
        this.resource = path[2] || 'list';
        this._id = path[3];
        this.event.broadcast('navigateModule', path);
    }

    /**
     *
     */
    init() {
        this.event.broadcast('changePageHeading', {
            title: this.title.module(this.module),
            breadcrumb: [{label: environment.name, active: true}, {
                label: this.title.resource(this.resource),
                active: true
            }]
        });

        this.list();
    }

    /**
     *
     */
    async list() {
        this.service.getAll(this._id, this.params).subscribe((data: any) => {
            this.data = data.data;
            this.selects = data.selects;
            this.limit = data.total;
            this.length = data.limit;
            this.loading = false;
        });
    }

    /**
     *
     */
    getSelect() {
        return this.selects;
    }

    /**
     *
     */
    search() {
        this.loading = true;
        this.list();
    }

    /**
     *
     * @param item
     */
    delete(item) {
        Swal.fire({
            title: 'Deletar registro?',
            text: 'Deseja realmente remover este registro agora?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0f7b6c',
            confirmButtonText: 'Sim, quero deletar!',
            cancelButtonText: 'Não',
            showLoaderOnConfirm: true,
            preConfirm: () => {
            }
        }).then((result) => {
            if (result.value) {
                this.service.delete(item._id).subscribe((success: any) => {
                    Swal.fire(
                        {
                            title: "Registro deletado!",
                            text: success.message,
                            type: 'success',
                            timer: success.timer,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        }
                    );
                }, error => {
                    Swal.fire(
                        {
                            title: "Ops!",
                            text: error,
                            type: 'error',
                            allowOutsideClick: false
                        }
                    );
                }, () => {
                    this.data = this.data.filter(d => d._id !== item._id);
                });
            }
        });
    }

    /**
     *
     */
    new() {
        this.router.navigate([`${this.module}/new`]);
    }

    /**
     *
     * @param id
     */
    edit(id) {
        this.router.navigate([`${this.module}/edit`, id]);
    }

    /**
     *
     */
    back() {
        this.router.navigate([`/${this.module}`]);
    }

    /**
     *
     */
    prev() {
        this.loading = true;
        this.params.page--;
        this.list();
    }

    /**
     *
     */
    next() {
        this.loading = true;
        this.params.page++;
        this.list();
    }

    /**
     *
     */
    first() {
        this.loading = true;
        this.params.page = 0;
        this.list();
    }

    /**
     *
     */
    last() {
        this.loading = true;
        this.params.page = (this.limit - 1);
        this.list();
    }

    /**
     *
     * @param size
     */
    changeSize(size) {
        this.params.page = 0;
        this.params.size = size;
        this.list();
    }
}
