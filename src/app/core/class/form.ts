import {Injectable} from '@angular/core';
import Swal from "sweetalert2";
import {environment} from "../../../environments/environment";
import { Router, ActivatedRoute } from '@angular/router';

export class Form {

    data: any;
    loading = true;
    editing = false;

    module = '';
    resource = '';

    cep: any = {};

    _save = true;
    _back = true;
    _upload = false;

    /**
     *
     * @param event
     * @param active
     * @param router
     * @param service
     * @param title
     */
    constructor(public event, public active: ActivatedRoute, public router: Router, public service, public title) {
        let path = this.router.routerState.snapshot.url.split('/');
        this.module = path[1];
        this.resource = path[2];
    }

    /**
     *
     */
    init() {

        this.active.paramMap.subscribe((result: any) => {
            if (!result.params.id && result.params.id === undefined) {
                this.loading = false;
            } else {
                this.editing = true;
                this.load(result.params.id);
            }
        });

        this.event.broadcast('changePageHeading', {
            title: this.title.module(this.module),
            breadcrumb: [{
                label: 'SBD',
                active: true
            }, {label: this.title.resource(this.resource), active: true}, {
                label: this.title.module(this.module),
                active: true
            }]
        });
    }

    /**
     *
     * @param id
     */
    load(id) {
        this.service.getById(id).subscribe((data) => {
            this.data = data;
            this.loading = false;
            this.getData();
        });
    }

    /**
     *
     */
    getData() {
        return this.data;
    }

    /**
     *
     * @param file
     * @param field
     */
    async upload(file: FileList, field) {
        let name = field.parentNode.parentNode.querySelector('.custom-file-label');
        field.disable = true;
        let filename = file[0].name;
        name.innerText = filename;
        this.data[field.dataset.name] = file[0].name;
        
        let ready = new FileReader();
        ready.onloadend = (e) => {
            this.data.attach = ready.result;
        };

        ready.readAsDataURL(file[0]);

        if (this._upload) {
            await this.service.upload(file[0]).subscribe((success) => {
                this.data[field.dataset.name] = success.data._id;
            }, (error) => {
                this.data[field.dataset.name] = null;
                field.disable = false;
            }, () => {
                field.disable = false;
            });
        }
    }

    /**
     *
     * @param id
     */
    async delete_attach(id, name) {
        if (this._upload) {
            await this.service.delete_attach(id).subscribe((success) => {
            }, (error) => {
            }, () => {
            });
        } else {
            this.data[name] = '';
            this.data.attach = '';
        }
    }

    /**
     *
     */
    save() {
        Swal.fire({
            title: "Salvando...",
            text: "Aguarde por favor",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        let request;
        this.lockUnlockForm(true);

        this.clearData();

        if (this.data._id && this.data._id !== undefined) {
            request = this.service.update(this.data._id, this.data);
        } else {
            request = this.service.save(this.data);
        }

        request.subscribe(
            (success: any) => {
                Swal.fire({
                    title: "Sucesso",
                    text: success.message,
                    type: 'success',
                    timer: success.timer,
                    confirmButtonColor: '#0f7b6c',
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            error => {
                Swal.fire({
                    title: `Oops!`,
                    text: error,
                    type: 'error',
                    confirmButtonColor: '#0f7b6c',
                    allowOutsideClick: false
                }).then(() => {
                    this.lockUnlockForm(false);
                });
            },
            () => {
                this.data = null;
                this.lockUnlockForm(false);
                this.router.navigate([`/${this.module}`]);
            });
    }

    /**
     *
     */
    clearData() {
        let data = {};
        Object.keys(this.data).filter(f => {
            if (this.data[f] !== null) {
                data[f] = this.data[f];
            }
        });
        this.data = data;
    }

    /**
     *
     */
    back() {
        this.router.navigate([this.module]);
    }

    /**
     *
     * @param event
     */
    consultCep(event) {
        if (event.target.value.length === 9) {
            this.lockUnlockForm(true);
            fetch(environment.cep + `${event.target.value}/json`).then(response => {
                return response.json();
            }).then(data => {
                this.cep = data;
                this.getCep();
                this.lockUnlockForm(false);
            }).catch(error => {
                this.lockUnlockForm(false);
            });
        }
    }

    /**
     *
     * @param lock
     */
    lockUnlockForm(lock) {
        document.querySelector('fieldset').disabled = lock;
    }

    /**
     *
     */
    getCep() {
        return this.cep;
    }

}
