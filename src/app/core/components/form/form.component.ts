import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    /**
     *
     */
    @Input('component') component;

    /**
     *
     * @param router
     */
    constructor(public router: Router) {}

    /**
     *
     */
    ngOnInit() {}

}
