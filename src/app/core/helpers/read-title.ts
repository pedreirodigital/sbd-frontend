import {Injectable} from '@angular/core';

@Injectable()
export class ReadTitle {

    /**
     *
     */
    constructor() {
    }

    /**
     *
     * @param name
     */
    module(name) {
        let getMenu = JSON.parse(localStorage.getItem('menu'));
        localStorage.setItem('path', name);
        return getMenu[name];
    }

    /**
     *
     * @param name
     */
    resource(name) {
        switch (name) {
            case 'new':
                return 'Cadastrar';
                break;
            case 'edit':
                return 'Editar';
                break;
            case 'list':
                return 'Listagem';
                break;
            case 'doctor':
                return 'Médico';
                break;
            case 'diagnosticos':
                return 'Diagnósticos';
                break;
        }
        return name;
    }
}
