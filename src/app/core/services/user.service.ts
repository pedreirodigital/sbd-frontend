import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Services} from "../class/services";

@Injectable({providedIn: 'root'})
export class UserService extends Services {

    name = 'user';

    /**
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }
}
