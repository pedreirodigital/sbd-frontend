import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Services} from "../class/services";

@Injectable({providedIn: 'root'})
export class QuizService extends Services {

    name = 'quiz';

    /**
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }


    /**
     *
     * @param id
     */
    delete(id) {
        return this.http.delete(`${environment.host}/${this.name}/admin/${id}`);
    }
    /**
     *
     */

    listAll(id, options = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a
            }, []).join('&');
        }
        return this.http.get(`${environment.host}/${this.name}/all/${id}${opt}`);
    }

    filters() {
        return this.http.get(`${environment.host}/${this.name}/filters`);
    }

    All(options = false) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null) {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a
            }, []).join('&')
        }
        return this.http.get(`${environment.host}/${this.name}/all${opt}`);
    }

    campaing(): any {
        return this.http.get(`${environment.host}/campaign/list`);
    }

    report(data, options) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null || options[k] !== '' || options[k] !== 'null') {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a
            }, []).join('&')
        }
        return this.http.post(`${environment.host}/${this.name}/report${opt}`, data);
    }

    search(options) {
        let opt = '';
        if (options) {
            opt = '?' + Object.keys(options).reduce((a, k) => {
                if (options[k] !== null) {
                    a.push(k + '=' + encodeURIComponent(options[k]));
                }
                return a
            }, []).join('&')
        }
        return this.http.get(`${environment.host}/${this.name}/reporting${opt}`);
    }
}
