import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Services} from "../class/services";

@Injectable({providedIn: 'root'})
export class HospitalService extends Services {

    name = 'hospital';

    /**
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }
}
