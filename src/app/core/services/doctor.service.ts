import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Services} from "../class/services";

@Injectable({providedIn: 'root'})
export class DoctorService extends Services {

    name = 'doctor';

    /**
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    /**
     *
     */

    getAll(id): any {
        return this.http.get(`${environment.host}/${this.name}/${id}`);
    }

    /**
     *
     * @param data
     */
    admin(data) {
        return this.http.put(`${environment.host}/${this.name}/${data.id}`, {admin: true});
    }

    /**
     *
     * @param id
     */
    getId(id): any {
        console.log(`${environment.host}/${this.name}/get/${id}`);
        return this.http.get(`${environment.host}/${this.name}/get/${id}`);
    }
}
