import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Services} from "../class/services";
import {environment} from "../../../environments/environment";

@Injectable({providedIn: 'root'})
export class CampaignService extends Services {

    name = 'campaign';

    /**
     *
     * @param http
     */
    constructor(http: HttpClient) {
        super(http);
    }

    list() {
        return this.http.get(`${environment.host}/${this.name}/list`);
    }
}
