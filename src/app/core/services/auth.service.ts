import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

import {CookieService} from '../services/cookie.service';
import {Auth} from '../models/auth.models';
import {environment} from '../../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    auth: Auth;

    constructor(private http: HttpClient, private cookieService: CookieService) {
    }

    /**
     * Returns the current user
     */
    public currentUser(): Auth {
        if (!this.auth) {
            this.auth = JSON.parse(this.cookieService.getCookie('doctor'));
        }
        return this.auth;
    }

    /**
     * Performs the auth
     * @param email email of user
     * @param password password of user
     */
    login(email: string, password: string) {
        return this.http.post<any>(`${environment.host}/login`, {email, password})
            .pipe(map(user => {
                if (user && user.token) {
                    this.auth = user.data;
                    this.auth.token = user.token;
                    this.cookieService.setCookie('doctor', JSON.stringify(this.auth), 1);
                }
                return user;
            }));
    }

    /**
     * Recover password user
     */

    recover(email) {
        return this.http.post<any>(`${environment.host}/login/recover`, {email})
            .pipe(map(data => {
                return data;
            }));
    }

    /**
     * Logout the user
     */
    logout() {
        this.cookieService.deleteCookie('doctor');
        this.auth = null;
    }
}

