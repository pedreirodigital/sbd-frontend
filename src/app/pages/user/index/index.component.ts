import {Component, OnInit, AfterViewInit} from '@angular/core';
import {EventService as Event} from '../../../core/services/event.service';
import {UserService as Service} from "../../../core/services/user.service";
import {Index} from "../../../core/class";
import {Router} from "@angular/router";
import {ReadTitle} from "../../../core/helpers/read-title";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})
export class UserIndexComponent extends Index implements OnInit, AfterViewInit {



    constructor(event: Event, service: Service, router: Router, title: ReadTitle) {
        super(event, service, router, title);
    }

    ngOnInit() {
        this.init();
    }

    ngAfterViewInit() {
    }
}
