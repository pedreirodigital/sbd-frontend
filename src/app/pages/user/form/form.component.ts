import {Component, OnInit} from '@angular/core';
import {EventService} from "../../../core/services/event.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService as Service} from "../../../core/services/user.service";
import {Form} from "../../../core/class/form";
import {ReadTitle} from "../../../core/helpers/read-title";

@Component({
    selector: 'app-user-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class UserFormComponent extends Form implements OnInit {

    data: any = {};
    loading = true;
    editing = false;

    constructor(event: EventService, active: ActivatedRoute, router: Router, service: Service, title: ReadTitle) {
        super(event, active, router, service, title);
    }

    ngOnInit() {
        this.init();
    }

    getData(): any {
        this.data.password = null;
        return super.getData();
    }
}
