import {Component, OnInit, AfterViewInit} from '@angular/core';
import {EventService as Event} from '../../../core/services/event.service';
import {DoctorService as Service} from "../../../core/services/doctor.service";
import {Index} from "../../../core/class";
import {ActivatedRoute, Router} from "@angular/router";
import {ReadTitle} from "../../../core/helpers/read-title";
import Swal from "sweetalert2";

@Component({
    selector: 'app-doctor',
    templateUrl: './doctor.component.html',
    styleUrls: ['./doctor.component.scss']
})
export class DoctorIndexComponent extends Index implements OnInit, AfterViewInit {

    _new = false;
    _edit = false;
    _admin = true;
    _delete = false;
    _back = true;
    _quiz = true;
    hospital_id = null;

    /**
     *
     * @param event
     * @param service
     * @param router
     * @param title
     */
    constructor(event: Event, service: Service, router: Router, title: ReadTitle, public active: ActivatedRoute) {
        super(event, service, router, title);
        this.hospital_id = this.active.snapshot.params.id;
    }


    /**
     *
     */
    ngOnInit() {
        this.init();
    }

    /**
     *
     */
    ngAfterViewInit() {
    }

    /**
     *
     * @param item
     */
    admin(item) {
        Swal.fire({
            title: 'Tornar médico administrador?',
            text: 'Ao tornar o médico um administrador ele poderá gerenciar seus questionarios?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0f7b6c',
            confirmButtonText: 'Sim, quero torná-lo!',
            cancelButtonText: 'Não',
            showLoaderOnConfirm: true,
            preConfirm: () => {
            }
        }).then((result) => {
            if (result.value) {
                this.service.admin(item).subscribe((success: any) => {
                    Swal.fire(
                        {
                            title: "Médico administrador!",
                            text: success.message,
                            type: 'success',
                            timer: success.timer,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        }
                    );
                    item.admin = true;
                }, error => {
                    Swal.fire(
                        {
                            title: "Ops!",
                            text: error,
                            type: 'error',
                            allowOutsideClick: false
                        }
                    );
                }, () => {

                });
            }
        });
    }

    /**
     *
     * @param id
     */
    diagnostic(item) {
        this.router.navigate([`diagnostics/d-${item._id}`]);
    }
}
