import {Component, OnInit} from '@angular/core';
import {EventService} from "../../../core/services/event.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HospitalService as Service} from "../../../core/services/hospital.service";
import {Form} from "../../../core/class/form";
import {ReadTitle} from "../../../core/helpers/read-title";

@Component({
    selector: 'app-user-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class HospitalFormComponent extends Form implements OnInit {

    data: any = {};
    loading = true;
    editing = false;

    /**
     *
     * @param event
     * @param active
     * @param router
     * @param service
     * @param title
     */
    constructor(event: EventService, active: ActivatedRoute, router: Router, service: Service, title: ReadTitle) {
        super(event, active, router, service, title);
    }

    /**
     *
     */
    ngOnInit() {
        this.init();
    }

    /**
     *
     */
    getData(): any {
        return super.getData();
    }

    /**
     *
     */
    getCep(): {} {
        this.data.place = this.cep.logradouro;
        this.data.district = this.cep.bairro;
        this.data.city = this.cep.localidade;
        this.data.state = this.cep.uf;
        return super.getCep();
    }
}
