import {Component, OnInit, AfterViewInit} from '@angular/core';
import {EventService as Event} from '../../../core/services/event.service';
import {HospitalService as Service} from "../../../core/services/hospital.service";
import {Index} from "../../../core/class";
import {Router} from "@angular/router";
import {ReadTitle} from "../../../core/helpers/read-title";
import Swal from "sweetalert2";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})
export class HospitalIndexComponent extends Index implements OnInit, AfterViewInit {

    _doctors = true;
    _send = true;
    _quiz = true;

    states = [];
    cities = [];

    /**
     *
     * @param event
     * @param service
     * @param router
     * @param title
     */
    constructor(event: Event, service: Service, router: Router, title: ReadTitle) {
        super(event, service, router, title);
    }

    /**
     *
     */
    async ngOnInit() {
        await this.load();
    }

    async load() {
        this.init();

    }

    /**
     *
     */
    ngAfterViewInit() {
        setTimeout(() => {
            this.getStates();
        }, 1000)
    }

    /**
     *
     * @param id
     */
    doctors(id) {
        this.router.navigate([`${this.module}/doctor/${id}`])
    }

    /**
     *
     * @param item
     */
    send(item) {
        item.sending = true;
        this.service.sendCode(item._id).subscribe(
            (success: any) => {
                Swal.fire({
                    title: "Sucesso",
                    text: success.message,
                    type: 'success',
                    timer: success.timer,
                    confirmButtonColor: '#0f7b6c',
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            error => {
                Swal.fire({
                    title: `Oops!`,
                    text: error,
                    type: 'error',
                    confirmButtonColor: '#0f7b6c',
                    allowOutsideClick: false
                }).then(() => {
                    item.sending = false;
                });
            },
            () => {
                item.sending = false;
            });
    }

    /**
     *
     */
    getStates() {
        if (this.selects.length === 0) {
            setTimeout(() => {
                this.getStates();
            }, 1000)
        }
        this.states = Array.from(new Set<any>(this.selects.map(c => c.state))).sort();
    }

    /**
     *
     * @param e
     */
    getCities(e) {
        this.cities = Array.from(new Set<any>(this.selects.filter(c => c.state === e.target.value).map(n => n.city))).sort();
    }

    /**
     *
     */
    search() {
        super.search();
    }

    /**
     *
     * @param id
     */
    diagnostic(item) {
        this.router.navigate([`diagnostics/s-${item._id}`]);
    }

}
