import { Component } from '@angular/core';
import { CampaignService } from 'src/app/core/services/campaign.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import {uniq, max} from 'lodash'
import Swal from "sweetalert2";
@Component({
    selector: 'app-export',
    templateUrl: './export.component.html',
    styleUrls: ['./export.component.scss']
})
export class ExportComponent {

    campaign_id = null;
    campaign = []
    constructor(public campaignSvc: CampaignService, public http: HttpClient) {
        this.campaignSvc.list().toPromise().then(res => {
            this.campaign = res.data;
        })
    }

    async export() {
        Swal.fire({
            title: "Aguarde",
            text: "Estamos carregando os dados",
            showConfirmButton: false,
            allowOutsideClick: false
        });
        let res: any = await this.http.post(environment.host + '/quiz/export', {}).toPromise()

        let maior = res.map(r => Object.keys(r).length)
        maior = maior.indexOf(max(maior))
        let dados_para_exportar = [];
        let campos = Object.keys(res[maior])

        res.forEach(r => {
            let obj = {};
            campos.forEach(c => {
                obj[c] = ['', null, 'null', undefined, 'undefined'].indexOf(r[c]) === -1 ? r[c] : '' 
                if(c === 'quiz_created_at') obj[c] = new Date(obj[c]).toLocaleString('pt-BR')
                if(c === 'quiz_synced_at') obj[c] = new Date(obj[c]).toLocaleString('pt-BR')
            })
            dados_para_exportar.push(obj)
        })

        new Angular5Csv(dados_para_exportar, 'Exportando', {
            showLabels: true,
            headers: campos
        });
        Swal.close()
    }

}
