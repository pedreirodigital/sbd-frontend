import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from "@angular/forms";
import { DashboardComponent } from './dashboard.component';
import { UIModule } from 'src/app/shared/ui/ui.module';


@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        UIModule
    ],
    exports: [DashboardComponent]
})
export class DashboardModule {
}
