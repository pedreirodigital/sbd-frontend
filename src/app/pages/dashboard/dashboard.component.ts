import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { EventService } from '../../core/services/event.service';
import { CampaignService } from "../../core/services/campaign.service";
import { HospitalService } from "../../core/services/hospital.service";
import { QuizService } from "../../core/services/quiz.service";
import { Chart } from 'chart.js';
import { FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';

export interface Deposit {
    value: string;
    viewValue: string;
}

export class depositCreate {
    title: string;
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
    constructor(private eref: ElementRef, public http: HttpClient, private eventService: EventService, public service: CampaignService, public serviceDesk: HospitalService, public srvQuiz: QuizService) {

    }

    campaings = [];

    states: any = [];
    cities: any = [];
    services: any = [];
    posto: any = {};
    data: any = [];

    filter = {
        campaign_id: null,
        state: null,
        city: null,
        hospital_id: null
    };

    diagnostics: any = [];
    campaing: any = null;
    type = null;
    consult = null;

    chart: any;
    canvas: any;
    ctx: any;

    retorno: any = {
        ficha: []
    };

    async ngOnInit() {
        this.eventService.broadcast('changePageHeading', {
            title: 'Dashboard',
            breadcrumb: [{ label: 'SBD', active: true }, { label: 'Dashboard', active: true }]
        });
    }

    async ngAfterViewInit() {
        this.service.list().subscribe((result) => {
            this.campaings = result.data;
        });
        this.serviceDesk.getAll().subscribe(async (result) => {
            this.data = result.data;
            this.getStates();
            this.services = result.data.map(n => {
                return { value: n._id, label: n.name }
            }).sort();
        });
    }

    getStates() {
        this.cities = [];
        this.states = Array.from(new Set<any>(this.data.map(c => c.state))).sort();

    }

    getCities(e) {
        this.cities = Array.from(new Set<any>(this.data.filter(c => c.state === e.target.value).map(n => n.city))).sort();

        if (this.filter.state === null) {
            this.services = Array.from(new Set<any>(this.data.map(n => {
                return { value: n._id, label: n.name }
            }))).sort();
        } else {
            this.services = Array.from(new Set<any>(this.data.filter(c => c.state === this.filter.state).map(n => {
                return { value: n._id, label: n.name }
            }))).sort();
        }
        this.filter.city = null
    }

    getServices() {
        if (this.filter.city === null) {
            this.services = Array.from(new Set<any>(this.data.filter(c => c.state === this.filter.state).map(n => {
                return { value: n._id, label: n.name }
            }))).sort();
        } else {
            this.services = Array.from(new Set<any>(this.data.filter(c => c.state === this.filter.state && c.city === this.filter.city).map(n => {
                return { value: n._id, label: n.name }
            }))).sort();
        }
        this.filter.hospital_id = null
    }

    report() {
        this.loading = true;
        this.srvQuiz.search(this.filter).subscribe(data => {
            this.retorno = data;
            this.loading = false;
        });
    }

    loading = false;
    get disable_search_btn() {
        return this.filter.campaign_id === null || this.loading
    }

    get label_search_btn() {
        return this.loading ? 'CARREGANDO...' : 'PESQUISAR'
    }

    select(item) {
        this.retorno.ficha = this.retorno.ficha.map(m => {
            m.selected = false;
            return m;
        });
        item.selected = true;
        this.type = item;
        setTimeout(() => {
            this.charts(item.chart);
        }, 1000)
    }

    get public_route() {
        return parent.location.pathname.indexOf('statistics') !== -1
    }

    countDiagnostic(item) {
        if (this.consult !== null) {
            return this.consult.filter(f => f[this.type.name] === item.value).length;
        } else {
            return 0;
        }
    }

    percentDiagnostic(item) {
        try {
            if (this.consult.length !== 0) {
                let total = this.consult.length;
                return Math.floor(this.consult.filter(f => f[this.type.name] === item.value).length / total * 100) + '%';
            } else {
                return `0%`;
            }
        } catch (e) {

        }
    }

    selectPosto() {
        this.diagnostics = [];
        this.posto = this.services.find(f => f.value === this.filter.hospital_id).label;
    }

    charts(chart) {
        this.canvas = document.getElementById('chart');
        this.ctx = this.canvas.getContext('2d');
        if (this.chart) {
            this.chart.reset();
        }
        this.chart = new Chart(this.ctx, {
            type: 'pie',
            data: chart,
            options: {
                responsive: true,

            }
        });
    }
}
