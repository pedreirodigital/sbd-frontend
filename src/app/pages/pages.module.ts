import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';

import {UIModule} from '../shared/ui/ui.module';
import {PagesRoutingModule} from './pages-routing.module';

import {UserIndexComponent} from "./user/index/index.component";
import {UserFormComponent} from './user/form/form.component';

import {HospitalIndexComponent} from "./hospital/index/index.component";
import {HospitalFormComponent} from "./hospital/form/form.component";
import {DoctorIndexComponent} from "./hospital/doctor/doctor.component";

import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {CoreModule} from "../core/core.module";
import {ListComponent} from "../core/components/list/list.component";

import {FormComponent} from "../core/components/form/form.component";
import {NgxMaskModule} from "ngx-mask";
import {DiagnosticsIndexComponent} from "./diagnostics/diagnostics.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DashboardModule } from './dashboard/dashboard.module';
import { ImportComponent } from './import/import.component';
import { ExportComponent } from './export/export.component';

@NgModule({
    declarations: [
        UserIndexComponent,
        UserFormComponent,

        ImportComponent,
        ExportComponent,

        HospitalIndexComponent,
        HospitalFormComponent,

        DoctorIndexComponent,
        DiagnosticsIndexComponent,

        ListComponent,
        FormComponent
    ],
    imports: [
        DashboardModule,
        CommonModule,
        NgbDropdownModule,
        UIModule,
        PagesRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        CoreModule,
        NgbModule,
        NgxMaskModule.forRoot()
    ]
})
export class PagesModule {
}
