import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DiagnosticosComponent as Component} from "./diagnosticos.component";

describe('QuizIndexComponent', () => {
    let component: Component;
    let fixture: ComponentFixture<Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
