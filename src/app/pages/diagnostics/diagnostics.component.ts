import {Component, OnInit, AfterViewInit} from '@angular/core';
import {EventService, EventService as Event} from '../../core/services/event.service';
import {QuizService as Service} from "../../core/services/quiz.service";
import {Index} from "../../core/class";
import {ActivatedRoute, Router} from "@angular/router";
import {ReadTitle} from "../../core/helpers/read-title";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {CampaignService} from "../../core/services/campaign.service";
import {HospitalService} from "../../core/services/hospital.service";
import {DoctorService} from "../../core/services/doctor.service";
import * as _ from 'lodash';

import Swal from "sweetalert2";
@Component({
    selector: 'diagnostics-quiz',
    templateUrl: './diagnostics.component.html',
    styleUrls: ['./diagnostics.component.scss']
})
export class DiagnosticsIndexComponent extends Index implements OnInit, AfterViewInit {

    /**
     *
     * @param event
     * @param service
     * @param router
     * @param title
     */

    campaigns: any = [];
    services: any = [];
    doctors: any = [];
    cities: any = [];
    states: any = [];
    filters: any = {};

    campaign = null;

    _new = false;
    id = null;
    diagnostic = null;
    posto = null;

    hasService = false;
    hasDoctor = false;

    hospital: any = {};
    breadcrumb: any = [];

    dataFilter = [];

    constructor(event: Event, service: Service, router: Router, title: ReadTitle,
                public active: ActivatedRoute,
                private modalService: NgbModal,
                public srvCampaing: CampaignService,
                private eventService: EventService,
                public srvPosto: HospitalService,
                public srvDoctor: DoctorService) {
        super(event, service, router, title);
        this.breadcrumb = [{label: 'SBD', active: true}, {label: 'Listagem', active: true}];
        if (this.active.snapshot.params.id !== undefined) {
            this.id = this.active.snapshot.params.id;
            this.checkViews();
            this._nav = false;
            this._limit = false;
        }
    }

    /**
     *
     */
    hide_filter = false;
    async checkViews() {
        if (this.id.indexOf('s-') !== -1) {
            this.hasService = true;
            this.params['hospital_id'] = this.id.replace('s-', '');
            this.hide_filter = true;
            await this.srvPosto.getById(this.params['hospital_id']).subscribe(d_service => {
                this.breadcrumb.push({label: d_service.name, active: true})

                if(this.campaigns.length === 1){
                    this.params.campaign_id = this.campaigns[0]._id
                    this.search()
                }
            });
        }

        if (this.id.indexOf('d-') !== -1) {
            this.hasDoctor = true;
            this.params['doctor_id'] = this.id.replace('d-', '');
            this.hide_filter = true;
            await this.srvDoctor.getId(this.params['doctor_id']).subscribe(d_doctor => {
                this.breadcrumb.push({label: d_doctor.name, active: true})

                if(this.campaigns.length === 1){
                    this.params.campaign_id = this.campaigns[0]._id
                    this.search()
                }
            });
        }
    }

    /**
     *
     */
    ngOnInit() {
        // @ts-ignore
        this.params.campaign_id = null;
        // @ts-ignore
        this.params.city = null;
        // @ts-ignore
        this.params.state = null;
        // @ts-ignore
        this.params.hospital_id = null;
        // @ts-ignore
        this.params.doctor_id = null;
        this.eventService.broadcast('changePageHeading', {
            title: 'Diagnósticos',
            breadcrumb: this.breadcrumb
        });
        if (this.id !== null) {
            this.loading = true;
            this.list();
        }
    }

    _service = []
    _doctor = []

    async list(): Promise<void> {

        if (this.hasDoctor && this.id !== null) {
            this.params['filter'] = 'doctor';
            this.params['doctor_id'] = this.id.replace('d-', '');
            ;
        }

        if (this.hasService && this.id !== null) {
            this.params['filter'] = 'service';
            this.params['hospital_id'] = this.id.replace('s-', '');
        }

        this.service.All(this.params).subscribe(async (data) => {
            if (this.id === null) {
                this.data = data.data.quiz;
                this._doctor = data.data.doctor
                this._service = data.data.hospital
                this.dataFilter = data.data;
            } else {
                this.data = data.data.quiz;
                this._doctor = data.data.doctor
                this._service = data.data.hospital
                this.dataFilter = data.data;
            }

            this.limit = data.total;
            this.length = data.limit;
            this.loading = false;
        });
    }

    /**
     *
     * @param item
     */
    filter(item) {
        if (this.hasService) {
            return item.hospital_id === this.params['hospital_id'];
        }
        if (this.hasDoctor) {
            return item.doctor_id === this.params['doctor_id'];
        }
    }

    /**
     *
     */
    ngAfterViewInit() {
        this.service.campaing().subscribe((data) => {
            this.campaigns = data.data;
            this.setCampaign()
            this.checkViews()
            this.service.filters().subscribe((data) => {
                this.filters = data;
                this.services = Array.from(new Set<any>(data.desks)).sort();
                this.services = _.orderBy(data.desks.map(n => {
                    return {value: n._id, label: n.name}
                }), ['label'], ['sac']);
                this.loading = false;
                if (this.id !== null && this.hasDoctor) {
                    this.services = _.orderBy(data.desks.map(n => {
                        return {value: n._id, label: n.name}
                    }), ['label'], ['sac']);
                }
            });
        });
    }

    /**
     *
     * @param content
     * @param item
     */
    open(content, item) {
        this.diagnostic = item;
        console.log(item)
        this.modalService.open(content).result.then((result) => {

        }, (reason) => {
            this.getDismissReason(reason)
        }).catch(err => {
            console.log('DEU ERRO', err)
        });
    }

    /**
     *
     * @param reason
     */
    private getDismissReason(reason: any) {
        if (reason === ModalDismissReasons.ESC) {
            this.diagnostic = null;
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            this.diagnostic = null;
        } else {
            this.diagnostic = null;
        }
    }

    /**
     *
     * @param item
     */
    showValues(item) {
        if (!item.data) {
            return this.diagnostic[item.name];
        } else {
            let badge = '';
            if (typeof this.diagnostic[item.name] === 'number') {
                let name = this.campaign.find(c => c.name === item.name).data.find(d => d.value === this.diagnostic[item.name]).label;
                badge += name;
            } else {
                Object.values(this.diagnostic[item.name]).map(m => {
                    let name = this.campaign.find(c => c.name === item.name).data.find(d => d.value === m).label;
                    badge += `${name}, `;
                });
            }
            return badge;
        }
    }

    /**
     *
     */
    getStates() {
        if (this.id === null) {
            this.cities = [];
            this.doctors = [];
            this.params['city'] = null;
            this.params['hospital_id'] = null;
            this.params['doctor_id'] = null;
        }
        try{
            this.states = Array.from(new Set<any>(this.filters.desks.map(c => c.state))).sort();

        }catch(e){

        }
    }

    /**
     *
     * @param e
     */
    getCities(e) {
        if (this.id === null) {
            this.doctors = [];
            this.params['city'] = null;
            this.params['hospital_id'] = null;
            this.params['doctor_id'] = null;
        }
        this.cities = Array.from(new Set<any>(this.filters.desks.filter(c => c.state === e.target.value).map(n => n.city))).sort();

        if (this.params['state'] === null) {
            this.services = _.orderBy(this.filters.desks.map(n => {
                return {value: n._id, label: n.name}
            }), ['label'], ['sac']);
        } else {
            this.services = _.orderBy(this.filters.desks.filter(f => f.state === this.params['state']).map(n => {
                return {value: n._id, label: n.name}
            }), ['label'], ['sac']);
        }
        this.params.city = null
    }

    /**
     *
     * @param e
     */
    getServices(e) {
        if (this.id === null) {
            this.services = [];
            this.doctors = [];
            this.params['doctor_id'] = null;
        }


        this.params['doctor_id'] = null;

        this.services = _.orderBy(this.filters.desks.filter(f => f.state === this.params['state'] && f.city === this.params['city']).map(n => {
            return {value: n._id, label: n.name}
        }), ['label'], ['sac']);

        this.params.hospital_id = null
    }

    /**
     *
     * @param e
     */
    getDoctors(e) {
        this.doctors = this.filters.doctors.filter(d => Object.values(d.hospital_id).indexOf(this.params['hospital_id']) !== -1);
    }

    /**
     *
     */
    setCampaign() {
        if (this.id === null) {
            this.states = [];
            this.cities = [];
            this.doctors = [];
            this.params['state'] = null;
            this.params['city'] = null;
            this.params['hospital_id'] = null;
            this.params['doctor_id'] = null;
        }
        // let campaign: any = this.campaigns.find(c => c._id === this.params['campaign_id']);
        let campaign: any = this.campaigns[0];
        this.campaign = campaign.quiz.question;
        this.getStates();
    }

    /**
     *
     */
    setService(item) {
        return this._service.find(s => s.id == item).name
        // return 'SERVIÇO'
        // return item.name;
    }

    setDoctor(item) {
        return this._doctor.find(s => s.id == item).name
        // return item.name;
    }

    formatDate(date){
        return new Date(date).toLocaleString('pt-BR')
    }
}
