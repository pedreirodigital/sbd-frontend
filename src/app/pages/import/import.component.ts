import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { QuizService as Service } from "../../core/services/quiz.service";
import { DomSanitizer } from '@angular/platform-browser';
import { orderBy } from 'lodash'

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.scss']
})
export class ImportComponent {
    selectedFile = null;
    diagnostics = []
    doctor: any = {}
    hospital = [];
    diagnostics_synced = []

    input = null;

    constructor(public http: HttpClient,
        private modalService: NgbModal,
        public domSanitizer: DomSanitizer,
        public service: Service) {

    }

    
    onFileChanged(event) {
        this.input = event.target
        this.selectedFile = event.target.files[0];
        const fileReader: any = new FileReader();
        fileReader.readAsText(this.selectedFile, "UTF-8");
        fileReader.onload = () => { this.readDiagnostics(fileReader) }
        fileReader.onerror = (error) => {
            console.log(error);
        }
    }


    readDiagnostics(result) {
        this.diagnostics = JSON.parse(result.result)
        this.diagnostics = this.diagnostics.map(d => {
            d.order = 0;
            return d
        })
        this.doctor._id = this.diagnostics[0].doctor_id
        this.diagnostics = orderBy(this.diagnostics, ['synced_at'], ['desc'])
        this.search()
    }

    newImport() {
        this.doctor = {};
        this.hospital = [];
        this.diagnostics = [];
        this.diagnostics_synced = []
        this.input.value = ''
    }

    get diagnosticos_json() {
        return this.diagnostics.length
        return 0
    }
    get diagnosticos_sincronizados() {
        return this.diagnostics_synced.length;
        return 0
    }
    get diagnosticos_sincronizar() {

        return this.diagnosticos_json - this.diagnosticos_sincronizados
        return 0
    }

    get desabilitar_botao_importar() {
        return this.diagnosticos_json - this.diagnosticos_sincronizados === 0
    }


    synced(d) {
        if (this.diagnostics_synced === undefined) return false
        return this.diagnostics_synced.some((d2: any) => d2.uuid === d.uuid)
    }

    async search() {
        let search: any = await this.http.post(`${environment.host}/quiz/import`, { search: this.doctor._id }).toPromise();
        if(search.doctor.photo === null) search.doctor.photo = '/assets/images/no-avatar.png'
        this.doctor = search.doctor
        this.hospital = search.hospital;
        this.diagnostics_synced = search.diagnostics;
    }

    diagnostic = {};
    open(content, item) {
        this.diagnostic = item;
        this.modalService.open(content).result.then((result) => {

        }, (reason) => {
            this.diagnostic = null;
        });
    }

    campaign = []
    ngAfterViewInit() {
        this.service.campaing().subscribe((data) => {
            this.campaign = data.data[0].quiz.question;
        });
    }
    showValues(item) {
        if (!item.data) {
            return this.diagnostic[item.name];
        } else {
            let badge = '';
            if (typeof this.diagnostic[item.name] === 'number') {
                let name = this.campaign.find(c => c.name === item.name).data.find(d => d.value === this.diagnostic[item.name]).label;
                badge += name;
            } else {
                badge = Object.values(this.diagnostic[item.name]).map(m => {
                    let name = this.campaign.find(c => c.name === item.name).data.find(d => d.value === m).label;
                    return `<p style="margin: 0px">${name}</p>`;
                }).join('');
            }
            return this.domSanitizer.bypassSecurityTrustHtml(badge);
        }
    }

    async import() {

        let search: any = await this.http.post(`${environment.host}/quiz/import`, { diagnostics: this.diagnostics.filter(d => !this.diagnostics_synced.some(d2 => d2.uuid === d.uuid)) }).toPromise();
        this.search()
    }

}
