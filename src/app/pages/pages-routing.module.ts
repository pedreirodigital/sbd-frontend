import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './dashboard/dashboard.component';

import {UserIndexComponent} from "./user/index/index.component";
import {UserFormComponent} from "./user/form/form.component";

import {HospitalIndexComponent} from "./hospital/index/index.component";
import {HospitalFormComponent} from "./hospital/form/form.component";
import {DoctorIndexComponent} from "./hospital/doctor/doctor.component";
import {DiagnosticsIndexComponent} from "./diagnostics/diagnostics.component";
import { ImportComponent } from './import/import.component';
import { ExportComponent } from './export/export.component';

const routes: Routes = [
    {path: '', component: DashboardComponent},

    {path: 'import', component: ImportComponent},
    {path: 'export', component: ExportComponent},

    {path: 'user', component: UserIndexComponent},
    {path: 'user/new', component: UserFormComponent},
    {path: 'user/edit/:id', component: UserFormComponent},

    {path: 'service-desk', component: HospitalIndexComponent},
    {path: 'service-desk/new', component: HospitalFormComponent},
    {path: 'service-desk/edit/:id', component: HospitalFormComponent},
    {path: 'service-desk/doctor/:id', component: DoctorIndexComponent},

    {path: 'diagnostics', component: DiagnosticsIndexComponent},
    {path: 'diagnostics/:id', component: DiagnosticsIndexComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
