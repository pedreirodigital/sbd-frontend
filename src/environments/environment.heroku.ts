export const environment = {
    production: false,
    host: 'https://sgi-api-sbd.herokuapp.com/api',
    cep: 'https://viacep.com.br/ws/',
    name: 'SBD',
    campaing_id: '5dcb4861129b1417f48768cb'
};
