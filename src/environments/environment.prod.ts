// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    // host: 'https://sgi-api-sbd.herokuapp.com/api',
    host: 'https://sbd-dezembro.parser-server.itarget.com.br/api',
    // host: 'http://192.168.0.2:8000/api',
    // host: 'http://localhost:8000/api',
    cep: 'https://viacep.com.br/ws/',
    name: 'SBD',
    campaing_id: '5dcb4861129b1417f48768cb'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
